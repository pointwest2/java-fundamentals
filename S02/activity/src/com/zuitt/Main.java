package com.zuitt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String[] fruits = {"apple", "avocado", "banana", "kiwi", "orange"};

        System.out.println("Fruits in stock: " + Arrays.toString(fruits));

        Scanner userInput = new Scanner(System.in);

        System.out.println("Which fruit would you like to get the index of?");
        String searchTerm = userInput.nextLine().toLowerCase().trim();
        System.out.println("Looking for " + searchTerm);

        int result = Arrays.binarySearch(fruits, searchTerm);
        System.out.println("The index of " + searchTerm + " is: " + result);

        HashMap<String, String> userA = new HashMap<>();

        userA.put("firstName", "John");
        userA.put("lastName", "Smith");

        System.out.println("User A:");
        System.out.println(userA);

        HashMap<String, String> userB = new HashMap<>();

        userB.put("firstName", "Jane");
        userB.put("lastName", "Doe");

        System.out.println("User B:");
        System.out.println(userB);

        ArrayList<HashMap<String, String>> users = new ArrayList<>();

        users.add(userA);
        users.add(userB);

        System.out.println("Current Users:");
        System.out.println(users);

        // Stretch Goal
        System.out.println("What user id would you like view the details?");
        int findUser = userInput.nextInt();
        System.out.println(users.get(findUser-1));

    }
}
