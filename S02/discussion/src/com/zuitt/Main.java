package com.zuitt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {

        /*
        [Section] Arrays
        - Arrays are objects that contain a fixed/limited number of values of a single data type
        - Unlike in other languages like JavaScript, the length of Java arrays are established when the array is created
        - Syntax
            dataType[] identifier = new dataType[numberOfElements];
            dataType[] identifier = { elementA, elementB, ... };
        */
        int[] intArray = new int[3];
        intArray[0] = 10;
        intArray[1] = 25;
        intArray[2] = 50;

        System.out.println("Result of array creation:");
        System.out.println(intArray[0]);

        // This is another way of declaring variables, however for consistency it would be best to add the brackets after the data type declaration
        String stringArray[] = new String[3];
        stringArray[0] = "John";
        stringArray[1] = "Jane";
        stringArray[2] = "Joe";

        System.out.println(stringArray[0]);

        int[] intArray2 = { 10, 25, 50 };

        System.out.println(intArray2[1]);

        // [Section] Array Methods
        // toString Method
        // The value to be printed out will be the reference to the memory of the application
        // The resulting series of characters is the memory allocated for the variable "intArray2"
        System.out.println("Result of printing arrays without the toString Method:");
        System.out.println(intArray2);
        // Using the toString() method will retrieve the actual value as a string
        // Using the "Arrays" class, we can gain access to it's methods
        System.out.println("Result of printing arrays with the toString Method:");
        System.out.println(Arrays.toString(intArray2));

        // sort Method
        Arrays.sort(stringArray);
        System.out.println("Result of Arrays.sort():");
        System.out.println(Arrays.toString(stringArray));

        // binarySearch Method
        String searchTerm = "Joe";
        int result = Arrays.binarySearch(stringArray, searchTerm);
        System.out.println("The index of " + searchTerm + " is: " + result);

        /*
        [Section] Multidimensional Arrays
        - Can be used to represent complex data that may/may not represent real world objects
        - Syntax
            dataType[][] identifier = new dataType[numberOfElements][numberOfElements];
        */
        String[][] classroom = new String[3][3];

        // First Row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Porthos";

        // Second Row
        classroom[1][0] = "Mickey";
        classroom[1][1] = "Donald";
        classroom[1][2] = "Goofy";

        // Third Row
        classroom[2][0] = "Harry";
        classroom[2][1] = "Ron";
        classroom[2][2] = "Hermione";

        System.out.println("Result of Multidimensional arrays:");
        System.out.println(classroom);
        // The "deepToString" method is used in placed of the "toString" method when dealing with multidimensional arrays
        System.out.println(Arrays.deepToString(classroom));

        /*
        [Section] ArrayLists
        - Array lists are resizable arrays that function similarly to how arrays work in other languages like JavaScript
        - Using the "new" keyword in creating an ArrayList does not require the dataType of the Array List to be defined to avoid repetition
        - Syntax
            ArrayList<dataType> identifier = new ArrayList<>();
        */
        ArrayList<String> students = new ArrayList<>();

        // ArrayList Methods
        // Adding elements
        // arrayListName.add(value);
        students.add("John");
        students.add("Paul");
        System.out.println("Result of add method in Array Lists:");
        System.out.println(students);

        // Retrieving elements
        // arrayListName.get(index);
        System.out.println("Result of get method in Array Lists:");
        System.out.println(students.get(0));

        // Changing elements
        // arrayListName.set(index, value);
        System.out.println("Result of set method in Array Lists:");
        students.set(1, "George");
        System.out.println(students.get(1));

        // Removing elements
        // arrayListName.remove(index);
        System.out.println("Result of remove method in Array Lists:");
        students.remove(1);
        System.out.println(students);

        // Clearing ArrayLists
        // arrayListName.clear();
        System.out.println("Result of clear method in Array Lists:");
        students.clear();
        System.out.println(students);

        // Getting the length of an Array List
        // arrayListName.size();
        System.out.println("Result of size method in Array Lists:");
        System.out.println(students.size());

        // ArrayLists with initialized values
        // ArrayList<dataType> identifier = new ArrayList<>(Arrays.asList(value, value));
        System.out.println("Result of Array Lists with initialized values:");
        ArrayList<String> employees = new ArrayList<>(Arrays.asList("June", "Albert"));
        System.out.println(employees);

        /*
        [Section] HashMaps
        - Most objects in Java are defined as Classes that contain a set of properties and methods, sometimes this might not be the case and creating dynamic objects may be the best option
        - Used if fields in an object are added dynamically
        - In Java, object "keys" are also referred to as "fields"
        - Offers flexibility because field:value pairs are assigned when needed
        - Syntax
            HashMap<fieldDataType, valueDataType> identifier = new HashMap<>();
        */
        HashMap<String, String> employeeRole = new HashMap<>();

        // Adding fields
        // hashMapName.put("field", value);
        employeeRole.put("Brandon", "Student");
        employeeRole.put("Alice", "Dreamer");
        System.out.println("Result of put method in HashMaps:");
        System.out.println(employeeRole);

        // Retrieving field values
        // hashMapName.get("field");
        System.out.println("Result of get method in HashMaps:");
        System.out.println(employeeRole.get("Alice"));

        // Removing elements
        // hashMapName.remove("field");
        System.out.println("Result of remove method in HashMaps:");
        employeeRole.remove("Brandon");
        System.out.println(employeeRole);

        // Retrieving hashmap keys/fields
        // hashMapName.keySet();
        System.out.println("Result of keySet method in HashMaps:");
        System.out.println(employeeRole.keySet());

        // HashMaps with integers
        HashMap<String, Integer> grades = new HashMap<>();

        grades.put("Joe", 89);
        grades.put("Jane", 93);

        System.out.println("Result of HashMaps with integer values:");
        System.out.println(grades);

        // HashMaps with Array Lists
        HashMap<String, ArrayList<Integer>> subjectGrades = new HashMap<>();

        ArrayList<Integer> gradesListA = new ArrayList<>(Arrays.asList(10, 15, 20));
        ArrayList<Integer> gradesListB = new ArrayList<>(Arrays.asList(5, 10, 15));

        subjectGrades.put("Joe", gradesListA);
        subjectGrades.put("Jane", gradesListB);

        System.out.println("Result of HashMaps with ArrayLists:");
        System.out.println(subjectGrades);
        System.out.println(subjectGrades.get("Joe"));
        System.out.println(subjectGrades.get("Joe").get(0));

    }
}
