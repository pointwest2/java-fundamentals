package classes;

import java.util.ArrayList;

public class Phonebook {
    // Arraylist declaration
    // syntax
    // access_modifier ArrayList<data_type> listName;
    private ArrayList<Page> allPages;

    // constructor
    public Phonebook(){
        this.allPages = new ArrayList<>();
    }

    //methods
    public void addPage(Page page){
        allPages.add(page);
    }

    public int checkExistingPages(){
        return allPages.size();
    }

    public void displayPages() {
        //.size()
        // returns the size or the number of elements
        if(checkExistingPages() == 0) {
            System.out.println("\n[Notification] You currently have no contacts. Add some now!");
        } else {
            System.out.println("\n[Notification] You have " + allPages.size() + " contact(s).");
            for (int i = 0; i < this.allPages.size(); i++) {
                // .get()
                // return the element at a given index from the specified Array
                System.out.println((i+1) + ". " + this.allPages.get(i).getName() + " - " +
                        this.allPages.get(i).getPhoneNumber());
            }
        }
    }

    public void updatePage (Page oldPage, Page updatedPage) {
        int foundPosition = findPagePosition(oldPage.name);
        if (foundPosition < 0) {
            System.out.println(oldPage.getName() + ", was not found.");
        } else if(findPagePosition(updatedPage.getName()) != -1) {
            System.out.println("Contact with name " + updatedPage.getName() + " already exists. Contact wasn't updated.");
        }

        this.allPages.set(foundPosition, updatedPage);
        System.out.println(oldPage.getName() + " was replaced with " + updatedPage.getName());
    }

    public void removePage (Page page) {
        this.allPages.remove(page);
    }

    public Page findContact(String name) {
        int position = findPagePosition(name);
        if (position >= 0) {
            return this.allPages.get(position);
        }
        return null;
    }

    private int findPagePosition(String contactName) {
        for (int i = 0; i < this.allPages.size(); i++) {
            Page contactPage = this.allPages.get(i);
            if (contactPage.getName().equalsIgnoreCase(contactName)) {
                return i;
            }

//            else if (contactPage.getName().contains(contactName)){
//                return i;
//            }
        }
        return -1;
    }
}