package classes;

public class Page {

    // fields
    // object properties
    public String name;
    public String address;
    public String email;
    public String phoneNumber;

    // constructor
    // sets default values on object creation
    public Page(String name, String address, String email, String phoneNumber){
        this.name = name;
        this.address = address;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    // methods/getters
    // methods used to return object values
    public String getName(){
        return name;
    }

    public String getAddress(){
        return address;
    }

    public String getEmail(){
        return email;
    }

    public String getPhoneNumber(){
        return phoneNumber;
    }
}
