package classes;

import java.util.Scanner;

// Add feature
    //[X] cancel operation
    //[X] invalid error showing for every input
    //[X] add current operation (add,remove,search) in message
    //[] shorten codes - use array and loops for while statements
    //[X] phone number accepts only numbers
    //[X] phone number validation to accept 8-11 digits only
    //[] email validation (regex, pattern class, match class)
    // references
    // https://www.tutorialspoint.com/validate-email-address-in-java
    // http://commons.apache.org/proper/commons-validator/apidocs/org/apache/commons/validator/routines/EmailValidator.html
    // https://howtodoinjava.com/regex/java-regex-validate-email-address/
// Search feature
    //[X] case insensitive query
    //[X] cancel operation
    //[] search function to match only partial term search
        // references
        // https://www.javacodeexamples.com/java-regex-case-insensitive-example/1498
        // https://www.javatpoint.com/java-regex
        // .contains
            // else if (contactPage.getName().contains(contactName)){
    //[X] allow another input if contact not found
// Edit feature
    //[]
// Delete feature
    //[X] case insensitive query
    //[X] cancel operation
    //[X] remove contact confirmation before deleting
    //[X] allow another input if contact not found
// Extra
    //[] console logo for app and name
    //[] if mispelling on input, allow "back" button to edit

public class Main {

    // import classes

    // used to get user input
    // A simple text scanner which can parse primitive types and strings using regular expressions.
    private static Scanner scanner = new Scanner(System.in);
    private static Phonebook phonebook = new Phonebook();

    public static void main(String[] args) {
        welcomeMessage();

        boolean isClosed = false;
        while(!isClosed){
            displayOptions();
            System.out.println("==========================\nWhat would you like to do?");
            //.next()
                //gets the next complete token from the scanner
                // matches the delimiter pattern
            String option = scanner.nextLine();

            switch (option) {
                case "1":
                    phonebook.displayPages();
                    break;
                case "2":
                    addContact();
                    break;
                case "3":
                    updateContact();
                    break;
                case "4":
                    removeContact();
                    break;
                case "5":
                    findContact();
                    break;
                case "6":
                    System.out.println("\nClosing app. Come back soon!");
                    isClosed = true;
                    break;
                default:
                    System.out.println("\n[Error] Invalid option entered. Try again.");
                    break;
            }
        }
    }

    private static void welcomeMessage(){
        System.out.println(
                "Welcome to Phonebook!\n" +
                "\nInstructions: Type the number of your choice option then press [Enter]."
        );
    }

    private static void displayOptions(){
        System.out.println(
                "\nOptions: [1] Display contacts - [2] Add contact - [3] Update contact - [4] Remove contact - [5] Search contact - [6] Close app"
        );
    }

    private static void addContact(){
        String name = "";
        String address = "";
        String email = "";
        String phoneNumber = "";

        while(name.isEmpty()){
            System.out.println("\n[Add Contact]\nType 'Exit' anytime to cancel the operation.\nEnter contact name:");
            name = scanner.nextLine().trim();
            if(name.isEmpty()){
                System.out.println("[Error] Please input a valid name");
            } else if(name.equalsIgnoreCase("exit")){
                System.out.println("\n[Notification] Adding operation cancelled");
                return;
            }
        }

        while(address.isEmpty()){
            System.out.println("Enter contact address:");
            address = scanner.nextLine().trim();
            if(address.isEmpty()){
                System.out.println("[Error] Please input a valid address");
            } else if(address.equalsIgnoreCase("exit")){
                System.out.println("\n[Notification] Adding operation cancelled");
                return;
            }
        }

        while(email.isEmpty()){
            System.out.println("Enter contact email:");
            email = scanner.nextLine().trim().toLowerCase();
            if(email.equalsIgnoreCase("exit")){
                System.out.println("\n[Notification] Adding operation cancelled");
                return;
            }else if(!email.matches("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$")) {
                System.out.println("[Error] Please input a valid email");
                email = "";
            } else if(email.isEmpty()){
                System.out.println("[Error] Please input a valid email");
            }
        }

        while(phoneNumber.isEmpty()){
            System.out.println("Enter contact phone number:");
            phoneNumber = scanner.nextLine().trim();

            if(phoneNumber.equalsIgnoreCase("exit")){
                System.out.println("\n[Notification] Adding operation cancelled");
                return;
            } else if (phoneNumber.matches("[0-9]{8,11}")){
                phonebook.addPage(new Page(name, address, email, phoneNumber));
                System.out.println("\n[Notification] New contact has been added");
                System.out.println("Name: " + name);
                System.out.println("Address: " + address);
                System.out.println("Email: " + email);
                System.out.println("Phone Number: " + phoneNumber);
            } else {
                System.out.println("[Error] Please input a valid 8-11 digit phone number");
                phoneNumber = "";
            }
        }
    }

    private static void updateContact () {
        String updateName = "";
        String newName = "";
        String newAddress = "";
        String newEmail = "";
        String newPhoneNumber = "";

        if(phonebook.checkExistingPages() == 0){
            System.out.println("\n[Notification] No contacts available");
            updateName = "exit";
        }

        while(updateName.isEmpty()){
            System.out.println("\n[Update Contact]\nType 'Exit' instead to cancel the operation.\nEnter existing contact name:");
            updateName = scanner.nextLine().trim();

            if(updateName.isEmpty()){
                System.out.println("[Error] Please input a valid name");
            } else if(updateName.equalsIgnoreCase("exit")){
                System.out.println("\n[Notification] Updating contact operation cancelled");
                return;
            } else {
                Page existingPage = phonebook.findContact(updateName);
                if (existingPage == null) {
                    System.out.println("\n[Notification] Contact not found");
                } else {
                    System.out.println("\n[Notification] Contact found");
                    System.out.println("Name: "+ existingPage.getName());
                    System.out.println("Address: " + existingPage.getAddress());
                    System.out.println("Email: " + existingPage.getEmail());
                    System.out.println("Phone Number: " + existingPage.getPhoneNumber());
                }
            }
        }

        while(newName.isEmpty()){
            System.out.println("\n[Update Contact]\nType 'Exit' anytime to cancel the operation.\nEnter contact name:");
            newName = scanner.nextLine().trim();
            if(newName.isEmpty()){
                System.out.println("[Error] Please input a valid name");
            } else if(newName.equalsIgnoreCase("exit")){
                System.out.println("\n[Notification] Adding operation cancelled");
                return;
            }
        }

        while(newAddress.isEmpty()){
            System.out.println("Enter contact address:");
            newAddress = scanner.nextLine().trim();
            if(newAddress.isEmpty()){
                System.out.println("[Error] Please input a valid address");
            } else if(newAddress.equalsIgnoreCase("exit")){
                System.out.println("\n[Notification] Adding operation cancelled");
                return;
            }
        }

        while(newEmail.isEmpty()){
            System.out.println("Enter contact email:");
            newEmail = scanner.nextLine().trim().toLowerCase();
            if(newEmail.equalsIgnoreCase("exit")){
                System.out.println("\n[Notification] Adding operation cancelled");
                return;
            } else if(!newEmail.matches("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$")) {
                System.out.println("[Error] Please input a valid email");
                newEmail = "";
            } else if(newEmail.isEmpty()){
                System.out.println("[Error] Please input a valid email");
            }
        }

        while(newPhoneNumber.isEmpty()){
            System.out.println("Enter contact phone number:");
            newPhoneNumber = scanner.nextLine().trim();

            if(newPhoneNumber.equalsIgnoreCase("exit")){
                System.out.println("\n[Notification] Adding operation cancelled");
                return;
            } else if (newPhoneNumber.matches("[0-9]{8,11}")){
                phonebook.updatePage(phonebook.findContact(updateName), new Page(newName, newAddress, newEmail, newPhoneNumber));
                System.out.println("\n[Notification] New contact has been added");
                System.out.println("Name: " + newName);
                System.out.println("Address: " + newAddress);
                System.out.println("Email: " + newEmail);
                System.out.println("Phone Number: " + newPhoneNumber);
            } else {
                System.out.println("[Error] Please input a valid 8-11 digit phone number");
                newPhoneNumber = "";
            }
        }
    }

    private static void removeContact () {
        String name = "";

        if(phonebook.checkExistingPages() == 0){
            System.out.println("\n[Notification] No contacts available");
            name = "exit";
        }

        while(name.isEmpty()){
            System.out.println("\n[Remove Contact]\nType 'Exit' instead to cancel the operation.\nEnter existing contact name:");
            name = scanner.nextLine().trim();

            if(name.isEmpty()){
                System.out.println("[Error] Please input a valid name");
            } else if (name.equalsIgnoreCase("exit")){
                System.out.println("\n[Notification] Remove contact operation cancelled");
            } else {

                Page existingPage = phonebook.findContact(name);
                if (existingPage != null){
                    System.out.println("Are you sure you wish to remove '" + existingPage.getName() + "' from your contacts? Y/N");
                    String confirmation = scanner.nextLine().trim();

                    switch(confirmation){
                        case "n":
                            System.out.println("\n[Notification] Contact removal operation cancelled");
                            break;
                        case "y":
                            phonebook.removePage(existingPage);
                            System.out.println("\n[Notification] Contact removed");
                            break;
                    }

                } else {
                    System.out.println("\n[Notification] Contact not found");
                    name = "";
                }

            }
        }
    }

    private static void findContact() {
        String name = "";

        if(phonebook.checkExistingPages() == 0){
            System.out.println("\n[Notification] No contacts available");
            name = "exit";
        }

        while(name.isEmpty()){
            System.out.println("\n[Search Contact]\nType 'Exit' instead to cancel the operation.\nEnter existing contact name:");
            name = scanner.nextLine().trim();

            if(name.isEmpty()){
                System.out.println("[Error] Please input a valid name");
            } else if(name.equalsIgnoreCase("exit")){
                System.out.println("\n[Notification] Finding contact operation cancelled");
            } else {
                Page existingPage = phonebook.findContact(name);
                if (existingPage == null) {
                    System.out.println("\n[Notification] Contact not found");
                    name = "";
                } else {
                    System.out.println("\n[Notification] Contact found");
                    System.out.println("Name: "+ existingPage.getName());
                    System.out.println("Address: " + existingPage.getAddress());
                    System.out.println("Email: " + existingPage.getEmail());
                    System.out.println("Phone Number: " + existingPage.getPhoneNumber());
                }
            }
        }
    }
}