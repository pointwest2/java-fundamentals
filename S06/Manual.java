/*
=====================================================================
PNT006 - S06 - Pillars of OOP in Java (Encapsulation and Abstraction)
=====================================================================
*/

/*

References
    What is Object Oriented Programming?
        https://www.educative.io/blog/object-oriented-programming

Definition of terms
    Application - Root folder for the application

*/

/* 
1. Create a new Java project named "discussion".
    Documents > PNT006 > S06 > discussion
*/

/*
2. Add initial code to demonstrate procedural programming.
    Application > src > com.zuitt > Main.java
*/

        public class Main {

            public static void main(String[] args) {

                /*
                [Section] Encapsulation
                - Encapsulation is the mechanism of wrapping the data (variables) and code acting on the data (methods) together as a single unit
                - Unlike in procedural programming languages like JavaScript where code is written when necessary following a specific flow of the script, Java separates its code into classes
                */

                String petAName = "Franky";
                String petAGender = "Female";
                String petAClassification = "Dog";
                int petAAge = 10;
                String petAAddress = "Manila, Philippines";
                String petASound = "Bark!";

                describePet(petAName, petAGender, petAClassification, petAAge);
                makeSound(petAName, petASound);

                String petBName = "Simba";
                String petBGender = "Male";
                String petBClassification = "Lion";
                int petBAge = 1;
                String petBAddress = "Pride Lands";
                String petBSound = "Rawr!";

                describePet(petBName, petBGender, petBClassification, petBAge);
                makeSound(petBName, petBSound);

            }

            public static void describePet(String name, String gender, String classification, int age) {
                System.out.println(name + " is a " + gender + " " + classification + " who is " + age + " years of age.");
            }

            public static void makeSound(String name, String sound) {
                System.out.println(name + " says " + sound);
            }
        }

        /*
        Important Note:
            - Refer to "references" section of this file to find the documentation for What is Object Oriented Programming?.
        */

/*
3. Create a "Pet" class.
    Application > src > com.zuitt > Pet.java(Class)
*/

        public class Pet {

            // Properties
            private String name;
            private String gender;
            private String classification;
            private int age;
            private String address;
            private String sound;

            // Constructors
            public Pet() {}

            public Pet(String name, String gender, String classification, int age, String address, String sound) {
                this.name = name;
                this.gender = gender;
                this.classification = classification;
                this.age = age;
                this.address = address;
                this.sound = sound;
            }

            // Getters and Setters

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getClassification() {
                return classification;
            }

            public void setClassification(String classification) {
                this.classification = classification;
            }

            public int getAge() {
                return age;
            }

            public void setAge(int age) {
                this.age = age;
            }

            public String getSound() {
                return sound;
            }

            public void setSound(String sound) {
                this.sound = sound;
            }

            // Methods
            public void describePet() {
                System.out.println(getName() + " is a " + getGender() + " " + getClassification() + " who is " + getAge() + " years of age.");
            }

            public void makeSound() {
                System.out.println(getName() + " says " + sound);
            }

        }

        /*
        Important Note:
            - You may auto generate the constructors, getters and setters to also have the students an appreciation of additional features provided by IntelliJ.
        */

/* 
4. Refactor the code in the "Main" class to use the "Pet" class instead.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class Main {

            public static void main(String[] args) {

                /*
                [Section] Encapsulation
                - Encapsulation is the mechanism of wrapping the data (variables) and code acting on the data (methods) together as a single unit
                - Unlike in procedural programming languages like JavaScript where code is written when necessary following a specific flow of the script, Java separates its code into classes
                */

                /*
                String petAName = "Franky";
                String petAGender = "Female";
                String petAClassification = "Dog";
                int petAAge = 10;
                String petAAddress = "Manila, Philippines";
                String petASound = "Bark!";

                describePet(petAName, petAGender, petAClassification, petAAge);
                makeSound(petAName, petASound);
                */

                /*
                String petBName = "Simba";
                String petBGender = "Male";
                String petBClassification = "Lion";
                int petBAge = 1;
                String petBAddress = "Pride Lands";
                String petBSound = "Rawr!";

                describePet(petBName, petBGender, petBClassification, petBAge);
                makeSound(petBName, petBSound);
                */

                Pet petA = new Pet("Franky", "Female", "Dog", 10, "Manila, Philippines", "Bark!");

                petA.describePet();
                petA.makeSound();

                Pet petB = new Pet("Simba", "Male", "Lion", 1, "Pride Lands", "Rawr!");
                petB.describePet();
                petB.makeSound();

            }

            /*
            public static void describePet(String name, String gender, String classification, int age) {
                System.out.println(name + " is a " + gender + " " + classification + " who is " + age + " years of age.");
            }

            public static void makeSound(String name, String sound) {
                System.out.println(name + " says " + sound);
            }
            */

        }

/*
5. Create a "Calculator" class.
    Application > src > com.zuitt > Calculator.java(Class)
*/

        /*...*/

    	public class Calculator {

            private String brand;
            private int price;

            public Calculator() {}
            
            public Calculator(String brand, int price) {
                this.brand = brand;
                this.price = price;
            }

            public String getBrand() {
                return brand;
            }

            public void setBrand(String brand) {
                this.brand = brand;
            }

            public int getPrice() {
                return price;
            }

            public void setPrice(int price) {
                this.price = price;
            }

            public void compute(String numA, String numB, String operation){

                double doubleNumA = Double.parseDouble(numA);
                double doubleNumB = Double.parseDouble(numB);

                if (operation.equalsIgnoreCase("add")) {
                    System.out.println(doubleNumA + doubleNumB);
                } else if (operation.equalsIgnoreCase("subtract")) {
                    System.out.println(doubleNumA - doubleNumB);
                } else if (operation.equalsIgnoreCase("multiply")) {
                    System.out.println(doubleNumA * doubleNumB);
                } else if (operation.equalsIgnoreCase("divide") && doubleNumA > 0 && doubleNumB > 0) {
                    System.out.println(doubleNumA / doubleNumB);
                } else {
                    System.out.println("Invalid number or operation provided");
                }

            }

            public void turnOff(){
                System.out.println("Closing " + getBrand() + " calculator.");
            }

        }

/*
6. Instantiate a "Calculator" class and invoke the "compute" method.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class Main {

            public static void main(String[] args) {

                /*...*/
                petB.makeSound();

                /*
                [Section] Abstraction
                - Abstraction is the process where all logic and code complexity is hidden from the users/developers which helps reduce confusion and allows them to focus on the "what" of things rather than the "how"
                - Objects cannot be instantiated from interfaces
                */

                Calculator myCalculator = new Calculator("Casio", 500);
                myCalculator.compute("23", "45", "divide");

            }

            /*...*/

        }

/*
7. Rename and refactor the "Calculator" class to "Casio".
    Application > src > com.zuitt > Calculator.java(Class)
*/

        Calculator.java + Right Click > Refactor > Rename

        /*
        Important Note:
            - By refactoring the file name from "Calculator" to "Casio" IntelliJ will be able to refactor all relevant code using the new file name to avoid errors in manual refactoring.
            - You can see that in the "Main" class, the "Calculator" class was renamed to "Casio".
            - This is done in preparation for the Interfaces discussion.
        */

/*
8. Create a "Calculator" interface.
    Application > src > com.zuitt > Calculator.java(Interface)
*/

        /*...*/

        public interface Calculator {

            // Abstract method names should be descriptive of what the method does
            void compute(String numA, String numB, String operation);
            void turnOff();

        }

        /*
        Important Note:
            - When creating interfaces abstract methods must be descriptive of what the methods do to help ease of development by describing as much information of what it does with very little context.
        */

/*
9. Create a "Sharp" class and implement the "Calculator" interface.
    Application > src > com.zuitt > Sharp.java(Class)
*/

        /* 9a. Implement the "Calculator" interface. */

            /*...*/

            public class Sharp implements Calculator {

            }

            /*
            Important Note:
                - The "implements" keyword denotes that the class must implement/have all the methods in the interface thats being implemented on it.
                - Upon implementation of the "Calculator" interface, you will notice that an error will be shown when you hover over the code above stating that "Class 'Sharp' must be either declared abstract or implement abstract method..." which indicates that this class must implement the same methods that were declared in the "Calculator" interface.
            */

        /* 9b. Add code to complete the "Sharp" Class and change the computed value into an integer instead of double. */

            /*...*/

            public class Sharp implements Calculator {

                private String brand;
                private int price;

                public Sharp() {}

                public Sharp(String brand, int price) {
                    this.brand = brand;
                    this.price = price;
                }

                public String getBrand() {
                    return brand;
                }

                public void setBrand(String brand) {
                    this.brand = brand;
                }

                public int getPrice() {
                    return price;
                }

                public void setPrice(int price) {
                    this.price = price;
                }

                public void compute(String numA, String numB, String operation){

                    int intNumA = Integer.parseInt(numA);
                    int intNumB = Integer.parseInt(numB);

                    if (operation.equalsIgnoreCase("add")) {
                        System.out.println(intNumA + intNumB);
                    } else if (operation.equalsIgnoreCase("subtract")) {
                        System.out.println(intNumA - intNumB);
                    } else if (operation.equalsIgnoreCase("multiply")) {
                        System.out.println(intNumA * intNumB);
                    } else if (operation.equalsIgnoreCase("divide") && intNumA > 0 && intNumB > 0) {
                        System.out.println(intNumA / intNumB);
                    } else {
                        System.out.println("Invalid number or operation provided");
                    }

                }

                public void turnOff(){
                    System.out.println("Closing " + getBrand() + " calculator.");
                }

            }

            /*
            Important Note:
                - You may also use the refactor option of IntelliJ to quickly refactor all the variables in the "compute" method for appreciation of the students.
                - Changing the compute function into an integer will show that even if the "Casio" and "Sharp" classes both have the same functions/methods, they may perform differently with the "Casio" class producing a double data type as the output and the "Sharp" class producing an integer data type as the output.
                - Interfaces are used to achieve total abstraction by just simply providing the name of the method and the parameters to be used without necessarily defining "how" the information was used only providing "what" is necessary to achieve the output.
                - Interfaces act as contracts that whenever a class implements an interface, it must have the methods defined in the interface.
            */

/*
10. Instantiate a "Sharp" class and invoke the "compute" method.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class Main {

            public static void main(String[] args) {

                /*...*/
                myCalculator.compute("23", "45", "divide");

                Sharp mySharp = new Sharp("Sharp", 1000);
                mySharp.compute("23","45", "divide");

            }

            /*...*/

        }

/*
11. Implement the "Calculator" interface in the "Casio" class.
    Application > src > com.zuitt > Casio.java(Class)
*/

        /*...*/

        public class Casio implements Calculator{
            /*...*/
        }

/*
12. Create a "useCalculator" function and invoke it.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class Main {

            public static void main(String[] args) {

                /*...*/
                mySharp.compute("23","45", "divide");

                // The useCalculator function can accept any class that implements the "Calculator" interface as an argument
                useCalculator(myCasio);
                useCalculator(mySharp);

            }

            /*
            ...

            public static void makeSound(String name, String sound) {
                ...
            }
            */

            // Interfaces also allow different classes that use the same implementation to be passed as an argument
            public static void useCalculator(Calculator calculator) {
                calculator.compute("64", "32" , "add");
                calculator.turnOff();
            }

        }

/*
12. Create a "ScientificCalculator" interface.
    Application > src > com.zuitt > ScientificCalculator.java(Interface)
*/

        /*...*/

        public interface ScientificCalculator {
            void computeRemainder(String numA, String numB);
        }

/*
13. Implement the "ScientificCalculator" interface.
*/

    /*
    13a. Implement the "ScientificCalculator" interface to the "Casio" class 
        Application > src > com.zuitt > Casio.java(Class)
    */

            /*...*/

            public class Casio implements Calculator, ScientificCalculator{

                /*...*/

                public void turnOff(){
                    /*...*/
                }

                /*
                [Section] Multiple interface implementation
                - Java allows for classes to implement multiple interfaces
                - In the real world, calculators may be classified into basic calculators and scientific calculators that implement computation of remainders using the modulo operator
                */
                public void computeRemainder(String numA, String numB) {
                    double doubleNumA = Double.parseDouble(numA);
                    double doubleNumB = Double.parseDouble(numB);

                    System.out.println(doubleNumA % doubleNumB);
                }

            }

    /*
    13b. Implement the "ScientificCalculator" interface to the "Sharp" class 
        Application > src > com.zuitt > Casio.java(Class)
    */

            /*...*/

            public class Sharp implements Calculator, ScientificCalculator{

                /*...*/

                public void turnOff(){
                    /*...*/
                }

                public void computeRemainder(String numA, String numB) {
                    int intNumA = Integer.parseInt(numA);
                    int intNumB = Integer.parseInt(numB);

                    System.out.println(intNumA % intNumB);
                }

            }

/*
14. Invoke the "computeRemainder" methods.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class Main {

            public static void main(String[] args) {

                /*...*/
                myCasio.compute("23", "45", "divide");
                myCasio.computeRemainder("38", "4");

                Sharp mySharp = new Sharp("Sharp", 1000);
                mySharp.compute("23","45", "divide");
                mySharp.computeRemainder("38", "4");

                useCalculator(myCasio);
                /*...*/

            }

            /*...*/

        }

/*
========
Activity
========
*/

/*
1. Create a new Java project named "activity".
    Documents > PNT006 > S06 > activity
*/

/*
2. Create a "Cellphone" interface.
    Application > src > com.zuitt > User.java(Class)
*/

    	/*...*/

        public interface Cellphone {
            
            void sendText(String message, String recipient);
            void playMusic(String songName);
            
        }

/*
3. Create a multiple classes that implement the "Cellphone" interface.
*/

    /*
    3a. Create a "Nokia" Class.
        Application > src > com.zuitt > Nokia.java(Class)
    */

        /*...*/

        public class Nokia implements Cellphone{

            private String model;
            private int price;
            private String supplier;

            public Nokia() {}

            public Nokia(String model, int price, String supplier) {
                this.model = model;
                this.price = price;
                this.supplier = supplier;
            }

            public String getModel() {
                return model;
            }

            public void setModel(String model) {
                this.model = model;
            }

            public int getPrice() {
                return price;
            }

            public void setPrice(int price) {
                this.price = price;
            }

            public String getSupplier() {
                return supplier;
            }

            public void setSupplier(String supplier) {
                this.supplier = supplier;
            }

            public void sendText(String message, String recipient) {
                System.out.println("Text message sent to: " + recipient);
                System.out.println(message);
            }

            public void playMusic(String songName) {
                System.out.println("The song " + songName + " is playing.");
            }

        }

    /*
    3b. Create a "Samsung" Class.
        Application > src > com.zuitt > Samsung.java(Class)
    */

        package com.zuitt;

        public class Samsung implements Cellphone{

            private String model;
            private int price;
            private String supplier;

            public Samsung () {}

            public Samsung(String model, int price, String supplier) {
                this.model = model;
                this.price = price;
                this.supplier = supplier;
            }

            public String getModel() {
                return model;
            }

            public void setModel(String model) {
                this.model = model;
            }

            public int getPrice() {
                return price;
            }

            public void setPrice(int price) {
                this.price = price;
            }

            public String getSupplier() {
                return supplier;
            }

            public void setSupplier(String supplier) {
                this.supplier = supplier;
            }

            public void sendText(String message, String recipient) {
                System.out.println("Text message sent to: " + recipient);
                System.out.println(message + "<3 :D");
            }

            public void playMusic(String songName) {
                System.out.println("The song " + songName + " is playing on Spotify.");
            }

        }

/*
4. Instantiate the "Nokia" and "Samsung" classes and invoke their methods.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class Main {

            public static void main(String[] args) {

                Nokia myPhone = new Nokia("3310", 100, "Zuitt Electronics");
                myPhone.sendText("Hello from the 90's", "John");
                myPhone.playMusic("Backstreet Boys - Shape of My Heart");

                Samsung myOtherPhone = new Samsung("Galaxy S5", 10000, "Market Suppliers");
                myOtherPhone.sendText("Hw r u doing today?", "Jane");
                myOtherPhone.playMusic("Taylor Swift - Love Story");

            }
            
        }

/*
5. Create a "Camera" interface.
    Application > src > com.zuitt > Camera.java(Interface)
*/

    	/*...*/

        public interface Camera {

            void takePicture();
            void takeVideo();

        }

/*
6. Implement the "Camera" interface in the "Samsung" class.
    Application > src > com.zuitt > Samsung.java(Class)
*/

        /*...*/

        public class Samsung implements Cellphone, Camera {

            /*...*/

            public void playMusic(String songName) {
                /*...*/
            }

            public void takePicture() {
                System.out.println("Smile for the camera!");
            }

            public void takeVideo() {
                System.out.println("Capturing video of your best moments.");
            }

        }

/*
7. Invoke the additional "Samsung" methods.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class Main {

            public static void main(String[] args) {

                /*...*/
                myOtherPhone.playMusic("Taylor Swift - Love Story");
                myOtherPhone.takePicture();
                myOtherPhone.takeVideo();

            }

        }