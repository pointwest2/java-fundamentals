package com.zuitt;

public class Main {

    public static void main(String[] args) {

        Nokia myPhone = new Nokia("3310", 100, "Zuitt Electronics");
        myPhone.sendText("Hello from the 90's", "John");
        myPhone.playMusic("Backstreet Boys - Shape of My Heart");

        Samsung myOtherPhone = new Samsung("Galaxy S5", 10000, "Market Suppliers");
        myOtherPhone.sendText("Hw r u doing today?", "Jane");
        myOtherPhone.playMusic("Taylor Swift - Love Story");
        myOtherPhone.takePicture();
        myOtherPhone.takeVideo();

    }
}
