package com.zuitt;

public class Samsung implements Cellphone, Camera {

    private String model;
    private int price;
    private String supplier;

    public Samsung () {}

    public Samsung(String model, int price, String supplier) {
        this.model = model;
        this.price = price;
        this.supplier = supplier;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public void sendText(String message, String recipient) {
        System.out.println("Text message sent to: " + recipient);
        System.out.println(message + "<3 :D");
    }

    public void playMusic(String songName) {
        System.out.println("The song " + songName + " is playing on Spotify.");
    }

    public void takePicture() {
        System.out.println("Smile for the camera!");
    }

    public void takeVideo() {
        System.out.println("Capturing video of your best moments.");
    }

}
