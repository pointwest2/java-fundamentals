package com.zuitt;

public class Pet {

    // Properties
    private String name;
    private String gender;
    private String classification;
    private int age;
    private String address;
    private String sound;

    // Constructors
    public Pet() {}

    public Pet(String name, String gender, String classification, int age, String address, String sound) {
        this.name = name;
        this.gender = gender;
        this.classification = classification;
        this.age = age;
        this.address = address;
        this.sound = sound;
    }

    // Getters and Setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    // Methods
    public void describePet() {
        System.out.println(getName() + " is a " + getGender() + " " + getClassification() + " who is " + getAge() + " years of age.");
    }

    public void makeSound() {
        System.out.println(getName() + " says " + sound);
    }

}
