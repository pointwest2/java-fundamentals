package com.zuitt;

public class Main {

    public static void main(String[] args) {

        /*
        [Section] Encapsulation
        - Encapsulation is the mechanism of wrapping the data (variables) and code acting on the data (methods) together as a single unit
        - Unlike in procedural programming languages like JavaScript where code is written when necessary following a specific flow of the script, Java separates its code into classes
        */

        /*
        String petAName = "Franky";
        String petAGender = "Female";
        String petAClassification = "Dog";
        int petAAge = 10;
        String petAAddress = "Manila, Philippines";
        String petASound = "Bark!";

        describePet(petAName, petAGender, petAClassification, petAAge);
        makeSound(petAName, petASound);
        */

        /*
        String petBName = "Simba";
        String petBGender = "Male";
        String petBClassification = "Lion";
        int petBAge = 1;
        String petBAddress = "Pride Lands";
        String petBSound = "Rawr!";

        describePet(petBName, petBGender, petBClassification, petBAge);
        makeSound(petBName, petBSound);
        */

        Pet petA = new Pet("Franky", "Female", "Dog", 10, "Manila, Philippines", "Bark!");

        petA.describePet();
        petA.makeSound();

        Pet petB = new Pet("Simba", "Male", "Lion", 1, "Pride Lands", "Rawr!");
        petB.describePet();
        petB.makeSound();

        /*
        [Section] Abstraction
        - Abstraction is the process where all logic and code complexity is hidden from the users/developers which helps reduce confusion and allows them to focus on the "what" of things rather than the "how"
        - Objects cannot be instantiated from interfaces
        */

        Casio myCasio = new Casio("Casio", 500);
        myCasio.compute("23", "45", "divide");
        myCasio.computeRemainder("38", "4");

        Sharp mySharp = new Sharp("Sharp", 1000);
        mySharp.compute("23","45", "divide");
        mySharp.computeRemainder("38", "4");

        // The useCalculator function can accept any class that implements the "Calculator" interface as an argument
        useCalculator(myCasio);
        useCalculator(mySharp);

    }

    /*
    public static void describePet(String name, String gender, String classification, int age) {
        System.out.println(name + " is a " + gender + " " + classification + " who is " + age + " years of age.");
    }

    public static void makeSound(String name, String sound) {
        System.out.println(name + " says " + sound);
    }
    */

    // Interfaces also allow different classes that use the same implementation to be passed as an argument
    public static void useCalculator(Calculator calculator) {
        calculator.compute("64", "32" , "add");
        calculator.turnOff();
    }

}