package com.zuitt;

public class Casio implements Calculator, ScientificCalculator{

    private String brand;
    private int price;

    public Casio() {}

    public Casio(String brand, int price) {
        this.brand = brand;
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void compute(String numA, String numB, String operation){

        double doubleNumA = Double.parseDouble(numA);
        double doubleNumB = Double.parseDouble(numB);

        if (operation.equalsIgnoreCase("add")) {
            System.out.println(doubleNumA + doubleNumB);
        } else if (operation.equalsIgnoreCase("subtract")) {
            System.out.println(doubleNumA - doubleNumB);
        } else if (operation.equalsIgnoreCase("multiply")) {
            System.out.println(doubleNumA * doubleNumB);
        } else if (operation.equalsIgnoreCase("divide") && doubleNumA > 0 && doubleNumB > 0) {
            System.out.println(doubleNumA / doubleNumB);
        } else {
            System.out.println("Invalid number or operation provided");
        }

    }

    public void turnOff(){
        System.out.println("Closing " + getBrand() + " calculator.");
    }

    /*
    [Section] Multiple interface implementation
    - Java allows for classes to implement multiple interfaces
    - In the real world, calculators may be classified into basic calculators and scientific calculators that implement computation of remainders using the modulo operator
    */
    public void computeRemainder(String numA, String numB) {
        double doubleNumA = Double.parseDouble(numA);
        double doubleNumB = Double.parseDouble(numB);

        System.out.println(doubleNumA % doubleNumB);
    }

}
