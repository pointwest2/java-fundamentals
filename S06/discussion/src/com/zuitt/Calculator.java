package com.zuitt;

public interface Calculator {

    // Abstract method names should be descriptive of what the method does
    void compute(String numA, String numB, String operation);
    void turnOff();

}
