package com.zuitt;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        try {
            runErrors();
        } catch (ArithmeticException e) {
            System.out.println("Cannot divide by zero.");
        } catch (NullPointerException e) {
            System.out.println("Cannot get the length of null.");
        } catch (NumberFormatException e) {
            System.out.println("Cannot parse non-numerical values.");
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Cannot get element of index that does not exist.");
        } catch (NewException e) {
            System.out.println("NewException was thrown.");
        }

    }

    public static void runErrors() throws ArithmeticException, NullPointerException, NumberFormatException, ArrayIndexOutOfBoundsException, NewException {

        Scanner appScanner = new Scanner(System.in);

        System.out.println("Choose an exception to handle:");
        System.out.println("[1]Arithmetic, [2]NullPointer, [3]NumberFormat, [4]ArrayIndexOutOfBounds, [5]NewException");
        int option = appScanner.nextInt();

        if(option == 1){
            System.out.println(47/0);
        } else if (option == 2) {
            String myString = null;
            System.out.println(myString.length());
        } else if (option == 3) {
            String myOtherString = "hello";
            System.out.println(Integer.parseInt(myOtherString));
        } else if (option == 4) {
            String[] myWords = {"Hello", "World"};
            System.out.println(myWords[2]);
        } else {
            throw new NewException("Error");
        }

    }
}

class NewException extends Exception {
    public NewException(String message){
        super(message);
    }
}
