/*
======================================
PNT006 - S04 - Java Exception Handling
======================================
*/

/*

References
    Java Exceptions
        https://www.tutorialspoint.com/java/java_exceptions.htm
    Java Scanner Class
        https://docs.oracle.com/javase/8/docs/api/java/util/Scanner.html    
    Exception Handling in Java
        https://www.javatpoint.com/exception-handling-in-java
    Java throws Keyword
        https://www.javatpoint.com/throws-keyword-and-difference-between-throw-and-throws
    Java throw Keyword
        https://www.javatpoint.com/throw-keyword
    Checked and Unchecked Exceptions in Java
        https://techvidvan.com/tutorials/java-checked-and-unchecked-exception/

Definition of terms
    Application - Root folder for the application

*/

/* 
1. Create a new Java project named "discussion".
    Documents > PNT006 > S04 > discussion
*/

/*
2. Add the initial code to capture user input.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class Main {

            public static void main(String[] args) {
                
                Scanner appScanner = new Scanner(System.in);

                System.out.println("Input a number:");
                int num = appScanner.nextInt();
                System.out.println("The number you entered is " + num);

                System.out.println("Input a decimal number:");
                double decimal = appScanner.nextDouble();
                System.out.println("The decimal number you entered is " + decimal);

                double total = num + decimal;
                System.out.println("The total of the two numbers is: " + total);

                System.out.println("Input a string:");

                // Solution 1
                appScanner.nextLine();
                String string = appScanner.nextLine();

                // Solution 2
                // Will only capture the next token and not a string with spaces
                // String string = appScanner.next();

                System.out.println("The string you entered is " + string);

            }

        }

/*
3. Explain how the next, nextInt, nextDouble and nextLine methods work.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class Main {

            public static void main(String[] args) {
                
                /*...*/

                System.out.println("Input a number:");
                // Creates a token in a single input stream
                // stream = 10
                int num = appScanner.nextInt();
                /*...*/

                System.out.println("Input a decimal number:");
                // Creates another token in the same stream
                // stream = 10 10.2
                double decimal = appScanner.nextDouble();
                /*...*/

                System.out.println("Input a string:");

                // Solution 1
                // Creates a next line in preparation for the next input
                // stream = 10 10.2\n
                // \n denotes a new line which is recognized in the next input
                appScanner.nextLine();
                // Takes in the next line of the stream
                String string = appScanner.nextLine();

                // Solution 2
                // Will only capture the next token and not a string with spaces
                // stream = 10 10.2 hello
                // String string = appScanner.next();

                System.out.println("The string you entered is " + string);

            }

        }

        /*
        Important Note:
            - This concept of how the Scanner class works is important to understand for the students since their capstone for this module will be manipulating user input through the console.
            - Refer to "references" section of this file to find the documentation for Java Scanner Class.
        */

/* 
4. Add more code to demonstrate Try-Catch-Finally Statements.
    Application > Terminal
*/

        /* 4a. Add the try catch statements. */

            /*...*/

            public class Main {

                public static void main(String[] args) {
                    
                    /*...*/

                    /*
                    System.out.println("Input a number:");
                    // Creates a token in a single input stream
                    // stream = 10
                    int num = appScanner.nextInt();
                    System.out.println("The number you entered is " + num);
                    */

                    /*
                    [Section] Try-Catch-Finally Statements
                    - Running the application and inputting a non-integer/non-double values will return an exception error and will eventually crash the application
                    - Using the Try-Catch-Finally statements will allow us to prevent the application from crashing and continue with the rest of the code execution
                    - Syntax
                        try {
                            // code to test
                        } catch (ExceptionClass errorParameter) {
                            // code to execute
                        } finally {
                            // code to run in case error occurs
                        }
                    */
                    int num = 0;

                    try {
                        System.out.println("Input a number:");
                        num = appScanner.nextInt();
                    } catch (Exception e) {
                        System.out.println("Invalid input.");
                        System.out.println(e);
                        e.printStackTrace();
                    }

                    System.out.println("The number you entered is " + num);

                    /*
                    System.out.println("Input a decimal number:");
                    // Creates another token in the same stream
                    // stream = 10 10.2
                    double decimal = appScanner.nextDouble();
                    System.out.println("The decimal number you entered is " + decimal);
                    */

                    double decimal = 0;

                    try {
                        System.out.println("Input a decimal number:");
                        decimal = appScanner.nextDouble();
                    } catch (Exception e) {
                        System.out.println("Invalid input.");
                        System.out.println(e);
                        e.printStackTrace();
                    }

                    System.out.println("The decimal number you entered is " + decimal);

                    /*...*/

                }

            }

            /*
            Important Note:
                - Remember to comment out the previous code and create Try-Catch statements separately for comparison.
                - The "Exception" superclass is used in Java to capture any kind of error in our code and allows access to methods and other information that could help solve errors.
                - The "catch" statement will respond based on the error occured
                - The variable "e" which is an accepted abbreviation for errors will store the details of the error.
                - Notice that using the Try-Catch statement will not crash the app and allow us to input a string.
                - Adding the Try-Catch statements will continue to print out the messages even if an error occurred.
                - Since an exception/error was "caught" notice that the "catch" statement for the decimal input also printed out the same message.
                - The solution to this will be done in the next step.
                - Refer to "references" section of this file to find the documentation for Java Exceptions.
            */

        /* 4b. Add conditions to the messages to prevent them from printing out even if an error occurs. */

            /*...*/

            public class Main {

                public static void main(String[] args) {
                    
                    /*...*/

                    if(num != 0){
                        System.out.println("The number you entered is " + num);
                    }

                    /*...*/

                    if(decimal != 0){
                        System.out.println("The decimal number you entered is " + decimal);
                    }

                    /*...*/

                }

            }

            /*
            Important Note:
                - This step will solve the problem in the previous step but would be better understood by developers when used with "Finally" statements
            */

        /*. 4c. Add "Finally" statements to make the code more efficient and readable. */

            /*...*/

            public class Main {

                public static void main(String[] args) {
                    
                    /*...*/

                    int num = 0;

                    try {
                        /*...*/
                    } catch (Exception e) {
                        /*...*/
                    } finally {
                        if(num != 0){
                            System.out.println("The number you entered is " + num);
                        }
                    }

                    /*...*/

                    double decimal = 0;

                    try {
                        /*...*/
                    } catch (Exception e) {
                        /*...*/
                    } finally {
                        if(decimal != 0){
                            System.out.println("The decimal number you entered is " + decimal);
                        }
                    }

                    /*...*/

                }

            }

            /*
            Important Note:
                - Even including Try-Catch-Finally statements will not prevent the app from crashing whenever an invalid input is provided.
                - Some of these errors maybe fixed using exception handling, however some errors may be prevented by capturing them using the front end application. 
            */

/*
5. Add more code to demonstrate multiple catch statements.
    Application > src > com.zuitt > Main.java
*/

    	/*...*/

        public class Main {

            public static void main(String[] args) {

                /*...*/

                int num = 0;
                
                try {
                    /*...*/
                } catch (InputMismatchException e) {
                    System.out.println("The value provided is not a number.");
                } catch (Exception e) {
                    /*...*/
                } finally {
                    /*...*/
                }

                /*...*/

                double decimal = 0;

                try {
                    /*...*/
                } catch (InputMismatchException e) {
                    System.out.println("The value provided is not a number.");
                } catch (Exception e) {
                    /*...*/
                } finally {
                    /*...*/
                }

                /*...*/

            }

        }

        /*
        Important Note:
            - By adding other catch statements, we can explicitely define how our application will perform based on the exceptions encountered.
            - Refer to "references" section of this file to find the documentation for Java Exception Handling.
        */

/*
6. Add more code to demonstrate Exception handling using throws and throw and User Defined Exceptions.
    Application > src > com.zuitt > Main.java
*/

        /* 6a. Create a function that will return multiple errors.*/

            /*...*/

            public class Main {

                public static void main(String[] args) {
                    /*...*/
                }

                /*
                - This method/function will encounter the following errors:
                    - int divideByZero = 10/0; => ArithmeticException
                    - System.out.println(text.length()); => NullPointerException
                - We use the "throws" keyword here to indicate what exceptions will be encountered and handled accordingly when invoked/called in the "main" method
                */
                public static void multipleExceptions(Scanner appScanner) {

                    System.out.println("Which error would you like to receive? [1]ArithmeticException, [2]NullPointerException");
                    int option = appScanner.nextInt();

                    if (option == 1){
                        int divideByZero = 10/0;
                    } else if (option == 2) {
                        String text = null;
                        System.out.println(text.length());
                    }

                }

            }

            /*
            Important Note:
                - This function will accept the "appScanner" variable from the main method when invoked to prevent another Scanner class declaration.
                - When option "1" is chosen this function will return an "ArithmeticException" and when option "2" is chosen this will return a "NullPointerException"
                - Run the application and choose both options to demonstrate that at this point, the errors aren't being handled.
            */

        /* 6b. Invoke/call the "multipleExceptions" function/method. */

            /*...*/

            public class Main {

                public static void main(String[] args) {

                    /*...*/

                    System.out.println("The string you entered is " + string);

                    multipleExceptions(appScanner);

                }

            }

        /* 6c. Create a Try-Catch statement that will capture the errors from the "multipleExceptions" function. */

    		/*...*/

    	    public class Main {

    	        public static void main(String[] args) {

    	            /*...*/

    	            System.out.println("The string you entered is " + string);

                    /*
                    [Section] Exception handling using "throws" and "throw"
                    - Exceptions can be handled within the same method/function but can also be handled using a different method/function with an existing solution
                    - The "throws" keyword can be used to indicate an exception that will be thrown/encountered when executed
                    */

                    try {
                        multipleExceptions(appScanner);
                    } catch (ArithmeticException e){
                        System.out.println("Value cannot be divided by zero.");
                    } catch (NullPointerException e) {
                        System.out.println("String can't be null");
                    }

    	        }

    	    }

    	    /*
            Important Note:
                - Refer to "references" section of this file to find the documentation for Java throws Keyword.
            */

        /* 6d. Create a user defined exception. */

            public class Main {
                /*...*/
            }

            /*
            [Section] User Defined Exceptions
            - Can be utilized to further handle exceptions that cannot be accomplished using pre-built exceptions
            - Inherits properties from the "Exception" class
            */
            class NewException extends Exception {
                public NewException(String message, Scanner appScanner){
                    super(message);
                    System.out.println("Give me another number:");
                    int myNum = appScanner.nextInt();
                    System.out.println(myNum);
                }
            }

            /*
            Important Note:
                - The "super" function is inherited from the "Exception" class.
                - The "extends" keyword allows the user defined exception to access everything that the Exception class has.
                - Classes and the other syntax here will be discussed further in the next session.
            */

        /* 6e. Create another Try-Catch statement to demonstrate the use of "throw". */

            /*...*/

            public class Main {

                public static void main(String[] args) {

                    /*...*/

                    System.out.println("The string you entered is " + string);

                    /*
                    [Section] Exception handling using "throws" and "throw"
                    ...
                    - The "throws" keyword can be used to indicate an exception that will be thrown/encountered when executed
                    - The "throw" keyword can be used in Try-Catch statements to handle responses to specific exceptions
                    */

                    /*
                    try {
                        multipleExceptions(appScanner);
                    } catch (ArithmeticException e){
                        System.out.println("Value cannot be divided by zero.");
                    } catch (NullPointerException e) {
                        System.out.println("String can't be null");
                    }
                    */

                    try {
                        System.out.println("Give me a number:");
                        int userInput = appScanner.nextInt();
                        if (userInput == 9){
                            throw new NewException("Your syntax is correct.", appScanner);
                        }
                    } catch (NewException e) {
                        System.out.println(e);
                    }

                }

            }

            /*...*/

            /*
            Important Note:
                - Refer to "references" section of this file to find the documentations for Java throw Keyword and Checked and Unchecked Exceptions in Java.
            */

/*
========
Activity
========
*/

/*
1. Create a new Java project named "activity".
    Documents > PNT006 > S04 > activity
*/

/*
2. Create a "runErrors" function that will take the user's input to generate the different commonly encountered exceptions.
    Application > src > com.zuitt > Main.java
*/

    	/*...*/

        public class Main {

            public static void main(String[] args) {

            }

            public static void runErrors() throws ArithmeticException, NullPointerException, NumberFormatException, ArrayIndexOutOfBoundsException {

                Scanner appScanner = new Scanner(System.in);

                System.out.println("Choose an exception to handle:");
                System.out.println("[1]Arithmetic, [2]NullPointer, [3]NumberFormat, [4]ArrayIndexOutOfBounds");
                int option = appScanner.nextInt();

                if(option == 1){
                    System.out.println(47/0);
                } else if (option == 2) {
                    String myString = null;
                    System.out.println(myString.length());
                } else if (option == 3) {
                    String myOtherString = "hello";
                    System.out.println(Integer.parseInt(myOtherString));
                } else if (option == 4) {
                    String[] myWords = {"Hello", "World"};
                    System.out.println(myWords[2]);
                }

            }

        }

        /*
        Important Note:
            - The "NumberFormatException" and "ArrayIndexOutOfBounds" errors were not demonstrated in the discussion.
            - Have the students refer to the "Exception Handling in Java" document under the "commonly encountered exceptions section" to find how these exceptions are generated.
        */

/*
3. Create a Try-Catch Statement that will handle the thrown exceptions from the "runErrors" function.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class Main {

            public static void main(String[] args) {

                try {
                    runErrors();
                } catch (ArithmeticException e) {
                    System.out.println("Cannot divide by zero.");
                } catch (NullPointerException e) {
                    System.out.println("Cannot get the length of null.");
                } catch (NumberFormatException e) {
                    System.out.println("Cannot parse non-numerical values.");
                } catch (ArrayIndexOutOfBoundsException e) {
                    System.out.println("Cannot get element of index that does not exist.");
                }

            }

            public static void runErrors() throws ArithmeticException, NullPointerException, NumberFormatException, ArrayIndexOutOfBoundsException {
                /*...*/
            }

        }

/*
4. Create a user defined exception.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class Main {
            /*...*/
        }

        class NewException extends Exception {
            public NewException(String message){
                super(message);
            }
        }

/*
5. Implement the NewException in the "main" and "runErrors" function.
    Application > src > com.zuitt > Main.java
*/

    	/*...*/

        public class Main {

            public static void main(String[] args) {

                try {
                    /*...*/
                } catch (ArithmeticException e) {
                    /*...*/
                } catch (NullPointerException e) {
                    /*...*/
                } catch (NumberFormatException e) {
                    /*...*/
                } catch (ArrayIndexOutOfBoundsException e) {
                    /*...*/
                } catch (NewException e) {
                    System.out.println("NewException was thrown.");
                }

            }

            public static void runErrors() throws ArithmeticException, NullPointerException, NumberFormatException, ArrayIndexOutOfBoundsException, NewException {

                /*...*/

                if(option == 1){
                    /*...*/
                } else if (option == 2) {
                    /*...*/
                } else if (option == 3) {
                    /*...*/
                } else if (option == 4) {
                    /*...*/
                } else {
                    throw new NewException("Error");
                }

            }

        }

        /*...*/