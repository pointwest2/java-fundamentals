package com.zuitt;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // [Section] Scanner Class
        Scanner appScanner = new Scanner(System.in);

        /*
        System.out.println("Input a number:");
        // Creates a token in a single input stream
        // stream = 10
        int num = appScanner.nextInt();
        System.out.println("The number you entered is " + num);
        */

        /*
        [Section] Try-Catch-Finally Statements
        - Running the application and inputting a non-integer/non-double values will return an exception and will eventually crash the application
        - Using the Try-Catch-Finally statements will allow us to prevent the application from crashing and continue with the rest of the code execution
        - Syntax
            try {
                // code to test
            } catch (ExceptionClass errorParameter) {
                // code to execute
            } finally {
                // code to run in case error occurs
            }
        */
        int num = 0;

        try {
            System.out.println("Input a number:");
            num = appScanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("The value provided is not a number.");
        } catch (Exception e) {
            System.out.println("Error!");
            // System.out.println(e);
            // e.printStackTrace();
        } finally {
            if(num != 0){
                System.out.println("The number you entered is " + num);
            }
        }

        /*
        System.out.println("Input a decimal number:");
        // Creates another token in the same stream
        // stream = 10 10.2
        double decimal = appScanner.nextDouble();
        System.out.println("The decimal number you entered is " + decimal);
        */

        double decimal = 0;

        try {
            System.out.println("Input a decimal number:");
            decimal = appScanner.nextDouble();
        } catch (InputMismatchException e) {
            System.out.println("The value provided is not a number.");
        } catch (Exception e) {
            System.out.println("Error");
            // System.out.println(e);
            // e.printStackTrace();
        } finally {
            if(decimal != 0){
                System.out.println("The decimal number you entered is " + decimal);
            }
        }

        System.out.println("Input a string:");

        // Solution 1
        // Creates a next line in preparation for the next input
        // stream = 10 10.2\n
        // \n denotes a new line which is recognized in the next input
        appScanner.nextLine();
        // Takes in the next line of the stream
        String string = appScanner.nextLine();

        // Solution 2
        // Will only capture the next token and not a string with spaces
        // stream = 10 10.2 hello
        // String string = appScanner.next();

        System.out.println("The string you entered is " + string);

        /*
        [Section] Exception handling using "throws" and "throw"
        - Exceptions can be handled within the same method/function but can also be handled using a different method/function with an existing solution
        - The "throws" keyword can be used to indicate an exception that will be thrown/encountered when executed
        - The "throw" keyword can be used in Try-Catch statements to handle responses to specific exceptions
        */

        /*
        try {
            multipleExceptions(appScanner);
        } catch (ArithmeticException e){
            System.out.println("Value cannot be divided by zero.");
        } catch (NullPointerException e) {
            System.out.println("String can't be null");
        }
        */

        try {
            System.out.println("Give me a number:");
            int userInput = appScanner.nextInt();
            if (userInput == 9){
                throw new NewException("Your syntax is correct.", appScanner);
            }
        } catch (NewException e) {
            System.out.println(e);
        }

    }

    /*
    - This method/function will encounter the following errors:
        - int divideByZero = 10/0; => ArithmeticException
        - System.out.println(text.length()); => NullPointerException
    - We use the "throws" keyword here to indicate what exceptions will be encountered and handled accordingly when invoked/called in the "main" method
    */
    public static void multipleExceptions(Scanner appScanner) throws ArithmeticException, NullPointerException {

        System.out.println("Which error would you like to receive? [1]ArithmeticException, [2]NullPointerException");
        int option = appScanner.nextInt();

        if (option == 1){
            System.out.println(10/0);
        } else if (option == 2) {
            String text = null;
            System.out.println(text.length());
        }

    }

}

/*
[Section] User Defined Exceptions
- Can be utilized to further handle exceptions that cannot be accomplished using pre-built exceptions
- Inherits properties from the "Exception" class
*/
class NewException extends Exception {
    public NewException(String message, Scanner appScanner){
        super(message);
        System.out.println("Give me another number:");
        int myNum = appScanner.nextInt();
        System.out.println(myNum);
    }
}