package com.zuitt;

public class Regular extends User {

    public Regular() {}

    public Regular(String name, String joinDate, boolean isAdmin) {
        super(name, joinDate, isAdmin);
    }

    public void login() {
        System.out.println("Purchase today and get a 25% discount on your shipping fee!");
    }

    public void checkout() {
        System.out.println("Please login before you can process your payment.");
    }

}
