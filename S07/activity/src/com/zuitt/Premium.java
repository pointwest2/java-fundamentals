package com.zuitt;

public class Premium extends User {

    public Premium() {
    }

    public Premium(String name, String joinDate, boolean isAdmin) {
        super(name, joinDate, isAdmin);
    }

    public void login() {
        System.out.println("View products online with free shipping!");
    }

    public void checkout() {
        System.out.println("Payment received. Thank you for your purchase!");
    }

}
