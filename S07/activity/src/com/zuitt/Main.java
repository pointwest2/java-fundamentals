package com.zuitt;

public class Main {

    public static void main(String[] args) {

        Admin adminAccount = new Admin("Jane Smith", "05-01-83", true, "Lead Developer");
        adminAccount.addProduct();

        Premium myAccount = new Premium("John Smith", "10-24-21", false);
        System.out.println("Result from a Premium user:");
        myAccount.login();
        myAccount.checkout();

        Regular unknownUser = new Regular("Joe Garcia", "02-24-90" ,false);
        System.out.println("Result from a Regular user:");
        unknownUser.login();
        unknownUser.checkout();

    }

}
