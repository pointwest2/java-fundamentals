
/*
====================================================================
PNT006 - S07 - Pillars of OOP in Java (Inheritance and Polymorphism)
====================================================================
*/

/*

References
    What is Object Oriented Programming?
        https://www.educative.io/blog/object-oriented-programming
    SOLID Principles
        https://www.baeldung.com/solid-principles

Definition of terms
    Application - Root folder for the application

*/

/* 
1. Create a new Java project named "discussion".
    Documents > PNT006 > S07 > discussion
*/

/*
2. Create an "Animal" class.
    Application > src > com.zuitt > Animal.java(Class)
*/

        /*...*/

        public class Animal {

            private String classification;
            private String dietType;

            public Animal() {}
            public Animal(String classification, String dietType) {
                this.classification = classification;
                this.dietType = dietType;
            }

            public String getClassification() {
                return classification;
            }

            public void setClassification(String classification) {
                this.classification = classification;
            }

            public String getDietType() {
                return dietType;
            }

            public void setDietType(String dietType) {
                this.dietType = dietType;
            }

            public void sleep(){
                System.out.println("This animal is sleeping.");
            }

        }

        /*
        Important Note:
            - Refer to "references" section of this file to find the documentation for What is Object Oriented Programming?.
        */

/*
3. Create a "Reptile" class.
    Application > src > com.zuitt > Reptile.java(Class)
*/

    	/*...*/

        /*
        [Section] Inheritance
        - Allows classes to inherit properties and methods from other classes which makes code reusable and efficient
        - The "extends" keyword defines that this class will inherit/extend properties and methods from the "Animal" class
        - With the use of inheritance the information is made manageable in a hierarchical order
        - Relationships between classes is called super/parent class and sub/child class
        */
        public class Reptile extends Animal {

            private String habitat;
            private boolean hasScales;

            public Reptile() {}

            public Reptile(String classification, String dietType, String habitat, boolean hasScales) {

                // The "Animal" class is considered to be the super/parent class
                // Using the "super()" function invokes the immediate parent class' constructor
                super(classification, dietType);
                this.habitat = habitat;
                this.hasScales = hasScales;

            }

            public String getHabitat() {
                return habitat;
            }

            public void setHabitat(String habitat) {
                this.habitat = habitat;
            }

            public boolean getHasScales() {
                return hasScales;
            }

            public void setHasScales(boolean hasScales) {
                this.hasScales = hasScales;
            }

            public void eat() {
                System.out.println("This animal is eating");
            }

        }

        /*
        Important Note:
            - When using IntelliJ's auto generation for getters and setters, sometimes the translation might be incorrect, notice that the getter/setter function created for the "hasScales" property is generated as isHasScales.
            - Always make sure to check auto generated syntax to avoid any naming concerns.
        */

/* 
4. Create a "Crocodile" class.
    Application > src > com.zuitt > Crocodile.java(Class)
*/

    	/*...*/

    	public class Crocodile extends Reptile {

    	    private String name;
    	    private int age;

    	    public Crocodile() {}

    	    public Crocodile(String classification, String dietType, String habitat, boolean hasScales, String name, int age) {
    	        super(classification, dietType, habitat, hasScales);
    	        this.name = name;
    	        this.age = age;
    	    }

    	    public String getName() {
    	        return name;
    	    }

    	    public void setName(String name) {
    	        this.name = name;
    	    }

    	    public int getAge() {
    	        return age;
    	    }

    	    public void setAge(int age) {
    	        this.age = age;
    	    }

    	    public void describePet() {
    	        System.out.println("Name: " + getName());
    	        System.out.println("Age: " + getAge());
    	        System.out.println("Classification: " + getClassification());
    	        System.out.println("Diet Type: " + getDietType());
    	        System.out.println("Habitat: " + getHabitat());
    	        System.out.println("Has Scales: " + getHasScales());
    	    }

    	    public void swim() {
    	        System.out.println("This crocodile is swimming.");
    	    }

    	}


/* 
5. Instantiate an object from the "Crocodile" class.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class Main {

            public static void main(String[] args) {

            	// The "Crocodile" class has inherited all the properties and methods from the "Animal" and "Reptile" classes
                Crocodile myPet = new Crocodile();
                myPet.setClassification("Reptile");
                myPet.setDietType("Carnivore");
                myPet.setHabitat("Fresh Water");
                myPet.setHasScales(true);
                myPet.setName("Drogon");
                myPet.setAge(7);

                myPet.describePet();
                myPet.swim();
                myPet.sleep();
                myPet.eat();

            }
            
        }

/* 
6. Create a "Driver" class.
    Application > src > com.zuitt > Driver.java(Class)
*/

        /*...*/

        public class Driver {

            private String name;
            private int age;

            public Driver() {}

            public Driver(String name, int age) {
                this.name = name;
                this.age = age;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getAge() {
                return age;
            }

            public void setAge(int age) {
                this.age = age;
            }

        }

/* 
7. Create a "Car" class.
    Application > src > com.zuitt > Car.java(Class)
*/

    	/*...*/

        public class Car {

            /*
            [Section] Composition and Inheritance
            - "Composition" allows modelling objects that are made up of other objects and is defined by "has a relationship" (e.g. A car has a driver)
            - "Inheritance" allows modelling an object that is a subset/subclass of other objects and is defined by "is a relationship" (e.g. A crocodile is a reptile)
            */
            private String make;
            private String model;
            private Driver driver;

            public Car() {};

            public Car(String make, String model, Driver driver) {
                this.make = make;
                this.model = model;
                this.driver = driver;
            }

            public void start() {
                System.out.println("The car is running.");
            }

        }

/* 
8. Instantiate an object from the "Car" class.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class Main {

            public static void main(String[] args) {

            	/*...*/
                myPet.eat();

                Driver myDriver = new Driver("Michael Schumacher", 52);
                Car myCar = new Car("Ferrari", "F430", myDriver);
                // The getName method is not available because the "Car" class did not inherit the "Driver" class but is composed of it
                myCar.getName();

            }
            
        }

        /*
        Important Note:
            - Make sure to comment out the "myCar.getName()" line to avoid any errors
        */

/* 
9. Create a "Turtle" class.
    Application > src > com.zuitt > Turtle.java(Class)
*/

    	/*...*/

    	/*
    	[Section] Polymorphism
    	- Polymorphism is derived from the greek words "poly" meaning many and "morph" means forms.
    	- It is the ability of an object to take many forms.
    	*/
    	public class Turtle extends Reptile {

    	    private String name;
    	    private int age;

    	    public Turtle() {}

    	    public Turtle(String classification, String dietType, String habitat, boolean hasScales, String name, int age) {
    	        super(classification, dietType, habitat, hasScales);
    	        this.name = name;
    	        this.age = age;
    	    }

    	    public String getName() {
    	        return name;
    	    }

    	    public void setName(String name) {
    	        this.name = name;
    	    }

    	    public int getAge() {
    	        return age;
    	    }

    	    public void setAge(int age) {
    	        this.age = age;
    	    }

    	    public void swim() {
    	        System.out.println("This turtle is swimming using it's limbs and webbed feet.");
    	    }

    	}

/* 
10. Instantiate an object from the "Turtle" class.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class Main {

            public static void main(String[] args) {

            	/*...*/
                // myCar.getName();

                Turtle myOtherPet = new Turtle("Reptile", "Herbivore", "Salt Water", false, "Rhaegal", 7);
                myOtherPet.swim();

            }
            
        }

/* 
11. Add more code to the "Crocodile" class to demonstrate Method overriding and overloading.
    Application > src > com.zuitt > Crocodile.java(Class)
*/

    	/*...*/

        public class Crocodile extends Reptile {

            /*...*/

            public void swim() {
                System.out.println("This crocodile is swimming using it's tail.");
            }

            /*
            [Section] Method Overriding and Overloading
            - Method Overriding (Dynamic Method Dispatch or Runtime Polymorphism) and Overloading (Static or compile-time polymorphism) are the two main types of polymorphism
            */

            // Method Overriding
            // Used to change the functionality of a method provided by a super/parent class to fit the needs of the current class
            // The function is overridden by replacing the definition of the method in the parent and child classes
            public void sleep () {
                System.out.println(getName() + " is sleeping.");
            }

            // Method Overloading
            // Used to change parameters and functionality to previously inherited methods
            public void eat(String food) {
                System.out.println(getName() + " is eating " + food + ".");
            }

        }

/* 
12. Create a "Human" interface.
    Application > src > com.zuitt > Human.java(Interface)
*/

    	/*...*/

    	public interface Human {

    	    void eat();
    	    void breathe();
    	    void sleep();

    	}

/* 
13. Create a "Person" class.
    Application > src > com.zuitt > Person.java(Class)
*/

    	/*...*/

    	public class Person implements Human{

    	    private String name;
    	    private int age;

    	    public Person() {}

    	    public Person(String name, int age) {
    	        this.name = name;
    	        this.age = age;
    	    }

    	    public String getName() {
    	        return name;
    	    }

    	    public void setName(String name) {
    	        this.name = name;
    	    }

    	    public int getAge() {
    	        return age;
    	    }

    	    public void setAge(int age) {
    	        this.age = age;
    	    }

    	    public void eat() {
    	        System.out.println(getName() + " is eating.");
    	    }

    	    public void breathe() {
    	        System.out.println(getName() + " takes a deep breath.");
    	    }

    	    public void sleep() {
    	        System.out.println(getName() + " is sleeping.");
    	    }

    	}

/* 
14. Instantiate an object from the "Person" class.
    Application > src > com.zuitt > Main.java
*/

    	/*...*/

    	public class Main {

    	    public static void main(String[] args) {

    	    	/*...*/
    	        myOtherPet.swim();

    	        /*
    	                [Section] Inheritance vs Interfaces
    	                - There are instances when objects must not be instantiated and creating classes out of them is not necessary
    	                - Creating interfaces instead to define what a class should be able to do is better than creating a class
    	                - In the example below, a human object is not necessary to be instantiated/created so creating an interface would be appropriate since that is the object that would be used in the program
    	                */
    	                Person me = new Person("John Smith", 31);
    	                me.eat();
    	                me.breathe();
    	                me.sleep();

    	    }
    	    
    	}

/* 
15. Create a course class to demonstrate the concept of Single Responsibility.
    Application > src > com.zuitt > Course.java (Class)
*/

        /*...*/

        public class Course {

            private String name;
            private String description;
            private String price;
            // Used as a temporary database to store a list of courses
            private ArrayList<String> courses = new ArrayList<>();

            public Course() {
            }

            public Course(String name, String description, String price) {
                this.name = name;
                this.description = description;
                this.price = price;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public ArrayList<String> getCourses() {
                return courses;
            }

            public void setCourses(ArrayList<String> courses) {
                this.courses = courses;
            }

            public String addCourse(String name) {
                courses.add(name);

                return name + " was successfully added";
            }

            public boolean containsCourse(String name) {
                return courses.contains(name);
            }

        }

/* 
16. Instantiate a course class to demonstrate the concept of Single Responsibility.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class Main {

            public static void main(String[] args) {

                /*...*/
                me.sleep();

                // [Section] Single Responsibility Principle
                Course course = new Course("HTML", "The basics of programming", "500");

                course.addCourse("HTML");

                System.out.println("Result of getCourses() method:");
                System.out.println(course.getCourses());
                System.out.println("Result of containsCourse method:");
                System.out.println(course.containsCourse("HTML"));

            }

        }

        /*
        Important Note:
            - Refer to "references" section of this file to find the documentation for SOLID principles.
        */

/* 
17. Add a formatText method in the Course class to demonstrate single responsibility.
    Application > src > com.zuitt > Course.java (Class)
*/

        /*...*/

        public class Course {

            /*...*/

            public boolean containsCourse(String name) {
                /*...*/
            }

            // This violates the concept of Single Responsibility because this class' responsibility is to manage our courses data and manipulate it
           public String formatText(String name) {
               return name.toLowerCase();
           }

        }

/* 
18. Create a TextFormat class to demonstrate single responsibility.
    Application > src > com.zuitt > TextFormat.java (Class)
*/

        /*...*/

        public class TextFormat {

            public TextFormat() {
            }

            public String formatText(String name) {
                return name.toLowerCase();
            }

        }

/* 
19. Create a TextFormat class to demonstrate single responsibility.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class Main {

            public static void main(String[] args) {

                /*...*/
                System.out.println(course.containsCourse("HTML"));

                TextFormat textFormatter = new TextFormat();
                System.out.println(textFormatter.formatText(course.getCourses().get(0)));

            }

        }

/* 
20. Show the trainees/students the Reptile class to demonstrate the open-closed principle.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class Reptile extends Animal {

            /*
            [Section] Open for extension closed for modification / Open-closed principle
            - By inheriting classes and adding properties/modifying the newly created class we are implementing the open-closed principle
            */
            private String habitat;
            private boolean hasScales;

            /*...*/

        }

/* 
20. Create a Caveman class to demonstrate the Liskov Substitution Principle.
    Application > src > com.zuitt > Caveman.java (Class)
*/

        /*...*/

        public class Caveman implements Human{

            private String name;
            private int age;

            public Caveman() {}

            public Caveman(String name, int age) {
                this.name = name;
                this.age = age;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getAge() {
                return age;
            }

            public void setAge(int age) {
                this.age = age;
            }

            public void eat() {
                System.out.println(getName() + " is eating with their hands.");
            }

            public void breathe() {
                System.out.println(getName() + " takes a deep breath.");
            }

            public void sleep() {
                System.out.println(getName() + " is sleeping in a cave.");
            }
        }

/* 
20. Create a doAllActions function and invoke it demonstrate the Liskov Substitution Principle.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class Main {

            public static void main(String[] args) {

                /*...*/
                System.out.println(textFormatter.formatText(course.getCourses().get(0)));

                // [Section] Liskov Substituation
                Caveman caveman = new Caveman("John Doe", 1000);

                System.out.println("Result of Liskov substitution:");
                doAllActions(me);
                doAllActions(caveman);

            }

            public static void doAllActions(Human human) {
                human.eat();
                human.breathe();
                human.sleep();
            }

        }

/* 
21. Create a CompanyEmployee interface to demonstrate the concept of Interface Segregation.
    Application > src > com.zuitt > CompanyEmployee.java (Interface)
*/

        /*...*/

        // [Section] Interface Segregation
        public interface CompanyEmployee {

            // cleanOffice is a task for maintenance staff
            void cleanOffice();

            // sortPapers is a task for admin staff
            void sortPapers();

        }

/* 
21. Create an AdminStaff interface to demonstrate the concept of Interface Segregation.
    Application > src > com.zuitt > AdminStaff.java (Interface)
*/

        public interface AdminStaff {
            // sortPapers is a task for admin staff
            void sortPapers();
        }

/* 
22. Create a MaintenanceStaff interface to demonstrate the concept of Interface Segregation.
    Application > src > com.zuitt > Maintenance.java (Interface)
*/

        public interface MaintenanceStaff {
            // cleanOffice is a task for maintenance staff
            void cleanOffice();
        }

/* 
23. Create a StartupEmployee class to demonstrate the concept of Interface Segregation.
    Application > src > com.zuitt > StartupEmployee.java (Class)
*/

        /*...*/

        public class StartupEmployee implements MaintenanceStaff, AdminStaff{

            public void cleanOffice(){
                System.out.println("The staff is cleaning the office.");
            }

            public void sortPapers(){
                System.out.println("The staff is sorting out the paperwork.");
            }

        }

/* 
24. Create a CorporateEmployee class to demonstrate the concept of Interface Segregation.
    Application > src > com.zuitt > CorporateEmployee.java (Class)
*/

        /*...*/

        public class CorporateEmployee implements AdminStaff{

            public void sortPapers() {
                System.out.println("The staff is sorting out papers and analyzing the data");
            }
        }

/* 
25. Create a StartupCompany class to demonstrate the concept of Dependency Inversion.
    Application > src > com.zuitt > StartupCompany.java (Class)
*/

        /*...*/

        // [Section] Dependency Inversion
        public class StartupCompany {

            // By assigning a StartupEmployee as a member of our company, we are creating a tight coupling between the StartupCompany class and the StartupEmployee class that all employees must be able to perform all tasks
            private StartupEmployee startupEmployee;

            public StartupCompany(StartupEmployee startupEmployee) {
               this.startupEmployee = startupEmployee;
            }

        }

/* 
25. Refactor the StartupCompany class to demonstrate the concept of Dependency Inversion.
    Application > src > com.zuitt > StartupCompany.java (Class)
*/

        // [Section] Dependency Inversion
        public class StartupCompany {

            // By assigning a StartupEmployee as a member of our company, we are creating a tight coupling between the StartupCompany class and the StartupEmployee class that all employees must be able to perform all tasks
        //    private StartupEmployee startupEmployee;
            private AdminStaff adminStaff;

        //    public StartupCompany(StartupEmployee startupEmployee) {
        //        this.startupEmployee = startupEmployee;
        //    }

            public StartupCompany(AdminStaff adminStaff) {
                this.adminStaff = adminStaff;
            }
        }

/* 
26. Instantiate a StartupCompany class to demonstrate the concept of Dependency Inversion.
    Application > src > com.zuitt > Main.java
*/

        /*
        [Section] Dependency Inversion
        - By passing an interface instead of a class allows flexibility in creating a startup company that can have an employee that can cleanOffice and sortPapers or sortPapers only.
        */
        StartupEmployee startupEmployee = new StartupEmployee();
        CorporateEmployee corporateEmployee = new CorporateEmployee();

        // StartupCompany startupCompany = new StartupCompany(startupEmployee);
        StartupCompany startupCompany = new StartupCompany(corporateEmployee);

/*
========
Activity
========
*/

/*
1. Create a new Java project named "activity".
    Documents > PNT006 > S07 > activity
*/

/* 
2. Create a "User" class.
    Application > src > com.zuitt > User.java(Class)
*/

    	/*...*/

        public class User {

            private String name;
            private String joinDate;
            private boolean isAdmin;

            public User() {}

            public User(String name, String joinDate, boolean isAdmin) {
                this.name = name;
                this.joinDate = joinDate;
                this.isAdmin = isAdmin;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getJoinDate() {
                return joinDate;
            }

            public void setJoinDate(String joinDate) {
                this.joinDate = joinDate;
            }

            public boolean isAdmin() {
                return isAdmin;
            }

            public void setIsAdmin(boolean admin) {
                isAdmin = admin;
            }

            public void login() {
                System.out.println("User has logged in.");
            }

            public void checkout() {
                System.out.println("Process payment");
            }

        }

/* 
3. Create an "Admin" class.
    Application > src > com.zuitt > User.java(Class)
*/

    	/*...*/

        public class Admin extends User {

            private String role;

            public Admin() {}

            public Admin(String name, String joinDate, boolean isAdmin, String role) {
                super(name, joinDate, isAdmin);
                this.role = role;
            }

            public String getRole() {
                return role;
            }

            public void setRole(String role) {
                this.role = role;
            }

            public void addProduct() {
                System.out.println(getName() + " just added another product.");
            }

        }

/*
4. Instantiate an "Admin" object and invoke it's methods.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class Main {

            public static void main(String[] args) {

                Admin adminAccount = new Admin("Jane Smith", "05-01-83", true, "Lead Developer");
                adminAccount.addProduct();

            }

        }

/*
5. Create a "Premium" class and override the methods.
    Application > src > com.zuitt > Premium.java(Class)
*/

    	/*...*/

        public class Premium extends User {

            public Premium() {
            }

            public Premium(String name, String joinDate, boolean isAdmin) {
                super(name, joinDate, isAdmin);
            }

            public void login() {
                System.out.println("View products online with free shipping!");
            }

            public void checkout() {
                System.out.println("Payment received. Thank you for your purchase!");
            }

        }

/*
6. Create a "Regular" class and override the methods.
    Application > src > com.zuitt > Regular.java(Class)
*/

        /*...*/

        public class Regular extends User {

            public Regular() {}

            public Regular(String name, String joinDate, boolean isAdmin) {
                super(name, joinDate, isAdmin);
            }

            public void login() {
                System.out.println("Purchase today and get a 25% discount on your shipping fee!");
            }

            public void checkout() {
                System.out.println("Please login before you can process your payment.");
            }

        }

/*
7. Instantiate the "Premium" and "Regular" objects and invoke their methods.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class Main {

            public static void main(String[] args) {

                /*...*/
                adminAccount.addProduct();

                Premium myAccount = new Premium("John Smith", "10-24-21", false);
                System.out.println("Result from a Premium user:");
                myAccount.login();
                myAccount.checkout();

                Regular unknownUser = new Regular("Joe Garcia", "02-24-90" ,false);
                System.out.println("Result from a Regular user:");
                unknownUser.login();
                unknownUser.checkout();

            }

        }