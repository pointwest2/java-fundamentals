package com.zuitt;

public class Car {

    /*
    [Section] Composition and Inheritance
    - "Composition" allows modelling objects that are made up of other objects and is defined by "has a relationship" (e.g. A car has a driver)
    - "Inheritance" allows modelling an object that is a subset/subclass of other objects and is defined by "is a relationship" (e.g. A crocodile is a reptile)
    */
    private String make;
    private String model;
    private Driver driver;

    public Car() {};

    public Car(String make, String model, Driver driver) {
        this.make = make;
        this.model = model;
        this.driver = driver;
    }

    public void start() {
        System.out.println("The car is running.");
    }

}
