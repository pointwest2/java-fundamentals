package com.zuitt;

/*
[Section] Inheritance
- Allows classes to inherit properties and methods from other classes which makes code reusable and efficient
- The "extends" keyword defines that this class will inherit/extend properties and methods from the "Animal" class
- With the use of inheritance the information is made manageable in a hierarchical order
- Relationships between classes is called super/parent class and sub/child class
*/
public class Reptile extends Animal {

    /*
    [Section] Open for extension closed for modification / Open-closed principle
    - By inheriting classes and adding properties/modifying the newly created class we are implementing the open-closed principle
    */
    private String habitat;
    private boolean hasScales;

    public Reptile (){}

    public Reptile(String classification, String dietType, String habitat, boolean hasScales) {

        // The "Animal" class is considered to be the super/parent class
        // Using the "super()" function invokes the immediate parent class' constructor
        super(classification, dietType);
        this.habitat = habitat;
        this.hasScales = hasScales;

    }

    public String getHabitat() {
        return habitat;
    }

    public void setHabitat(String habitat) {
        this.habitat = habitat;
    }

    public boolean getHasScales() {
        return hasScales;
    }

    public void setHasScales(boolean hasScales) {
        this.hasScales = hasScales;
    }

    public void eat() {
        System.out.println("This animal is eating");
    }

}
