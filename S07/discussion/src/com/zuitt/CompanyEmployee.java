package com.zuitt;

// [Section] Interface Segregation
public interface CompanyEmployee {

    // cleanOffice is a task for maintenance staff
    void cleanOffice();

    // sortPapers is a task for admin staff
    void sortPapers();

}
