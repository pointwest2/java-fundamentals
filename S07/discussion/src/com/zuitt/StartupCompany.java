package com.zuitt;

// [Section] Dependency Inversion
public class StartupCompany {

    // By assigning a StartupEmployee as a member of our company, we are creating a tight coupling between the StartupCompany class and the StartupEmployee class that all employees must be able to perform all tasks
//    private StartupEmployee startupEmployee;
    private AdminStaff adminStaff;

//    public StartupCompany(StartupEmployee startupEmployee) {
//        this.startupEmployee = startupEmployee;
//    }

    public StartupCompany(AdminStaff adminStaff) {
        this.adminStaff = adminStaff;
    }
}


