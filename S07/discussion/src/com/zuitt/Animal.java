package com.zuitt;

public class Animal {

    private String classification;
    private String dietType;

    public Animal() {}
    public Animal(String classification, String dietType) {
        this.classification = classification;
        this.dietType = dietType;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getDietType() {
        return dietType;
    }

    public void setDietType(String dietType) {
        this.dietType = dietType;
    }

    public void sleep(){
        System.out.println("This animal is sleeping.");
    }

}
