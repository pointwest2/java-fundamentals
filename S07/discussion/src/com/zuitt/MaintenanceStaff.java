package com.zuitt;

public interface MaintenanceStaff {
    // cleanOffice is a task for maintenance staff
    void cleanOffice();
}
