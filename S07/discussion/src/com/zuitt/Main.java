package com.zuitt;

import org.w3c.dom.Text;

public class Main {

    public static void main(String[] args) {

        // The "Crocodile" class has inherited all the properties and methods from the "Animal" and "Reptile" classes
        Crocodile myPet = new Crocodile();
        myPet.setClassification("Reptile");
        myPet.setDietType("Carnivore");
        myPet.setHabitat("Fresh Water");
        myPet.setHasScales(true);
        myPet.setName("Drogon");
        myPet.setAge(7);

        myPet.describePet();
        myPet.swim();
        myPet.eat("chicken");
        myPet.sleep();

        Driver myDriver = new Driver("Michael Schumacher", 52);
        Car myCar = new Car("Ferrari", "F430", myDriver);
        // The getName method is not available because the "Car" class did not inherit the "Driver" class but is composed of it
        // myCar.getName();

        Turtle myOtherPet = new Turtle("Reptile", "Herbivore", "Salt Water", false, "Rhaegal", 7);
        myOtherPet.swim();

        /*
        [Section] Inheritance vs Interfaces
        - There are instances when objects must not be instantiated and creating classes out of them is not necessary
        - Creating interfaces instead to define what a class should be able to do is better than creating a class
        - In the example below, a human object is not necessary to be instantiated/created so creating an interface would be appropriate since that is the object that would be used in the program
        */
        Person me = new Person("John Smith", 31);
        me.eat();
        me.breathe();
        me.sleep();

        // [Section] Single Responsibility Principle
        Course course = new Course("HTML", "The basics of programming", "500");

        course.addCourse("HTML");

        System.out.println("Result of getCourses() method:");
        System.out.println(course.getCourses());
        System.out.println("Result of containsCourse method:");
        System.out.println(course.containsCourse("HTML"));

        TextFormat textFormatter = new TextFormat();
        System.out.println(textFormatter.formatText(course.getCourses().get(0)));

        // [Section] Liskov Substitution
        Caveman caveman = new Caveman("John Doe", 1000);

        System.out.println("Result of Liskov substitution:");
        doAllActions(me);
        doAllActions(caveman);

        /*
        [Section] Dependency Inversion
        - By passing an interface instead of a class allows flexibility in creating a startup company that can have an employee that can cleanOffice and sortPapers or sortPapers only.
        */
        StartupEmployee startupEmployee = new StartupEmployee();
        CorporateEmployee corporateEmployee = new CorporateEmployee();

        // StartupCompany startupCompany = new StartupCompany(startupEmployee);
        StartupCompany startupCompany = new StartupCompany(corporateEmployee);

    }

    public static void doAllActions(Human human) {
        human.eat();
        human.breathe();
        human.sleep();
    }

}


