package com.zuitt;

/*
[Section] Objects, Classes and Instances
- An object is the representation of real world objects. It is composed of states(properties) and behaviors(methods) that provides information about an object and what actions it can perform.
- A class is a blueprint from which objects are created from.
- An instance is a single and unique unit of a class that describes the relationship of the object, and it's class
- Syntax
    accessModifier class ClassName {

        // Property
        accessModifier dataType property_name;

        // Constructor
        // Empty Constructor
        accessModifier ClassName(){}

        // Parametrized Constructor
        accessModifier ClassName(dataType parameter_name){
            this.property_name = parameter_name
        }

        // Getters
        accessModifier returnDataType getPropertyName() {
            return this.property_name
        }

        // Setters
        accessModifier void setPropertyName(dataType parameter_name) {
            this.property_name = parameter_name
        }

        // Methods
        accessModifier returnDataType methodName(){
            // code to be executed
        }
    }
*/
public class Car {

    /*
    [Section] Properties
    - Defines the list of properties that an object would have
    - The "private" access modifier ensures that the properties may only be retrieved using the getters and updated only by it's setters
    */
    private String name;
    private String brand;
    private int manufactureDate;
    private String owner;

    /*
    [Section]Constructors
    - Constructors define the values of an object's properties when instantiated
    - The "public" method allows other classes that use the class to create instances of the object
    - Empty constructors are not necessary and are created by the Java compiler if no constructors are present, however some features of Java would require us to include them thus having an empty constructor is still good practice
    */

    // Empty Constructor
    public Car(){}

    // Parameterized Constructor
    public Car(String name, String brand, int manufactureDate, String owner){
        this.name = name;
        this.brand = brand;
        this.manufactureDate = manufactureDate;
        this.owner = owner;
    }

    /*
    [Section] Getters and Setters
    - Allows our objects to become read and write/read only or write only depending on the need
    - Getters and setters are not required and are optional
    - The use of getters and setters along with access modifiers ensure that information may only be retrieved and updated by classes that have access to it
    */

    // Getters
    public String getName(){
        return this.name;
    }
    public String getBrand(){
        return this.brand;
    }
    public int getManufactureDate(){
        return this.manufactureDate;
    }
    public String getOwner(){
        return this.owner;
    }

    // Setters
    public void setName(String name){
        this.name = name;
    }
    public void setBrand(String brand){
        this.brand = brand;
    }
    public void setYearOfMake(int manufactureDate){
        this.manufactureDate = manufactureDate;
    }

    /*
    [Section] Methods
    - Functions that belong to this class that allow objects to perform different functions
    */
    public void drive(){
        System.out.println("The car is running");
    }
    public void printDetails(){
        System.out.println("This " + getManufactureDate() + " " + getBrand() + " " + getName() + " is owned by: " + getOwner() + ".");
    }

}
