package com.zuitt;

import com.zuitt.accessmodifiers.Example;
import com.zuitt.accessmodifiers.Test;

public class Main {

    public static void main(String[] args) {

        // Instantiates a new Car object using the Car class
	    Car myCar = new Car();
        System.out.println(myCar);

        /*
        myCar.name = "Civic";
        myCar.brand = "Honda";
        myCar.year_of_make = 1998;
        System.out.println(myCar.name);
        System.out.println(myCar.brand);
        System.out.println(myCar.manufactureDate);
        */

        myCar.setName("Civic");
        myCar.setBrand("Honda");
        myCar.setYearOfMake(1998);
        System.out.println(myCar.getName());
        System.out.println(myCar.getBrand());
        System.out.println(myCar.getManufactureDate());

        // Adding properties to objects cannot be done unless defined in the Class
        // myCar.mileage = 100_000;

        // Initial property values of objects may not be assigned without creating constructors
        Car otherCar = new Car("Charger", "Dodge", 1970, "Dominic Toretto");
        /*
        System.out.println(otherCar.name);
        System.out.println(otherCar.brand);
        System.out.println(otherCar.manufactureDate);
        */

        System.out.println(otherCar.getName());
        System.out.println(otherCar.getBrand());
        System.out.println(otherCar.getManufactureDate());
        System.out.println(otherCar.getOwner());
        // otherCar.owner = "John Smith";

        otherCar.drive();
        otherCar.printDetails();

        Example accessModifier = new Example();
        System.out.println(accessModifier.message);
        // Since the Main class is not found inside the "accessmodifiers" package, it cannot access the "anotherMessage" property
        // System.out.println(accessModifier.defaultMessage);

        // The "privateMessage" property can only be accessed within it's class
        // System.out.println(accessModifier.privateMessage);
        // The message can only be printed out because the "printPrivateMessage" method has access to the "privateMessage" class
        accessModifier.printPrivateMessage();

        Test defaultAccess = new Test();
        // The "anotherMessage" property can be accessed by the "Test" class which allows us to print the message in the "Main" class
        defaultAccess.accessingDefault();

        Protected protectedAccess = new Protected();
        protectedAccess.printProtectedMessage();

    }

    // The "extends" keyword allows to create a parent-child/sub class relationship between the "Example" and "Protected" class
    public static class Protected extends Example {
        public void printProtectedMessage() {
            System.out.println(protectedMessage);
        }
    }

}