/*
==============================
PNT006 - S05 - Java OOP Basics
==============================
*/

/*

References
    What is Object Oriented Programming?
        https://www.educative.io/blog/object-oriented-programming
    Class vs Object vs Instance
        https://alfredjava.wordpress.com/2008/07/08/class-vs-object-vs-instance/
    Differences between classes, objects and instances
        https://stackoverflow.com/questions/1215881/the-difference-between-classes-objects-and-instances
    Java Access Modifiers
        https://www.javatpoint.com/access-modifiers
    Java Access Modifiers Table
        https://www.geeksforgeeks.org/access-modifiers-java/

Definition of terms
    Application - Root folder for the application

*/

/* 
1. Create a new Java project named "discussion".
    Documents > PNT006 > S05 > discussion
*/

/*
2. Create a "Car" class to demonstrate instantiation of objects.
*/

        /*
        2a. Create a "Car" class. 
            Application > src > com.zuitt > Car.java(Class)
        */

                /*...*/

                /*
                [Section] Objects, Classes and Instances
                - An object is the representation of real world objects. It is composed of states(properties) and behaviors(methods) that provides information about an object and what actions it can perform.
                - A class is a blueprint from which objects are created from.
                - An instance is a single and unique unit of a class that describes the relationship of the object, and it's class
                - Syntax
                    accessModifier class ClassName {

                        // Property
                        accessModifier dataType property_name;

                    }
                */
                public class Car {

                    /*
                    [Section] Properties
                    - Defines the list of properties that an object would have
                    - The "private" access modifier ensures that the properties may only be retrieved using the getters and updated only by it's setters
                    */
                    public String name;
                    public String brand;
                    public int manufactureDate;

                }

                /*
                Important Note:
                    - Refer to "references" section of this file to find the documentations for What is Object Oriented Programming, Class vs Object vs Instance and Differences between classes, objects and instances.
                */

        /*
        2b. Instantiate a "Car" object.
            Application > src > com.zuitt > Main.java
        */

                /*...*/

                public class Main {

                    public static void main(String[] args) {

                        // Instantiates a new Car object using the Car class
                        Car myCar = new Car();
                        System.out.println(myCar);

                        myCar.name = "Civic";
                        myCar.brand = "Honda";
                        myCar.year_of_make = 1998;
                        System.out.println(myCar.name);
                        System.out.println(myCar.brand);
                        System.out.println(myCar.manufactureDate);
                        
                        // Adding properties to objects cannot be done unless defined in the Class
                        // myCar.mileage = 100_000;

                    }

                }

/*
3. Add more code to demonstrate the use of constructors.
*/

        /*
        3a. Instantiate a "Car" object.
            Application > src > com.zuitt > Main.java
        */

                /*...*/

                public class Main {

                    public static void main(String[] args) {

                        /*...*/
                        // myCar.mileage = 100_000;

                        // Initial property values of objects may not be assigned without creating constructors
                        Car otherCar = new Car("Charger", "Dodge", 1970);
                        System.out.println(otherCar.name);
                        System.out.println(otherCar.brand);
                        System.out.println(otherCar.manufactureDate);

                    }

                }

        /*
        3b. Add a constructor. 
            Application > src > com.zuitt > Car.java(Class)
        */

                /*...*/

                public class Car {

                    /*...*/
                    public int year_of_make;

                    /*
                    [Section]Constructors
                    - Constructors define the values of an object's properties when instantiated
                    - The "public" method allows other classes that use the class to create instances of the object
                    - Empty constructors are not necessary and are created by the Java compiler if no constructors are present, however some features of Java would require us to include them thus having an empty constructor is still good practice
                    */

                    // Empty Constructor
                    public Car(){}

                    // Parameterized Constructor
                    public Car(String name, String brand, int manufactureDate){
                        this.name = name;
                        this.brand = brand;
                        this.year_of_make = manufactureDate;
                    }

                }

/* 
4. Add more code to demonstrate the use of getters and setters.
*/

        /*
        4a. Change the access modifiers of the properties to private and create getters and setters.
            Application > src > com.zuitt > Car.java(Class)
        */

                /*...*/

                public class Car {

                    private String name;
                    private String brand;
                    private int manufactureDate;

                    // Empty Constructor
                    public Car(){}

                    /*...*/

                    public Car(String name, String brand, int manufactureDate){
                        /*...*/
                    }

                    /*
                    [Section] Getters and Setters
                    - Allows our objects to become read and write/read only or write only depending on the need
                    - Getters and setters are not required and are optional
                    - The use of getters and setters along with access modifiers ensure that information may only be retrieved and updated by classes that have access to it
                    */

                    // Getters
                    public String getName(){
                        return this.name;
                    }
                    public String getBrand(){
                        return this.brand;
                    }
                    public int getYearOfMake(){
                        return this.manufactureDate;
                    }

                    // Setters
                    public void setName(String name){
                        this.name = name;
                    }
                    public void setBrand(String brand){
                        this.brand = brand;
                    }
                    public void setYearOfMake(int manufactureDate){
                        this.manufactureDate = manufactureDate;
                    }

                }

                /*
                Important Note:
                    - The "private" access modifier for properties are more appropriate than "public" to ensure that the values of the object may not be changed without accessing it's getters and setters.
                    - The use of getters and setters are the appropriate approach of manipulating object properties when dealing with an OOP language.
                    - Getters and setters may also be automatically generated by doing the following:
                        Right click + Generate
                */

        /*
        4b. Add more code to use getters and setters for retrieving and instantiating object properties.
            Application > src > com.zuitt > Main.java
        */

                /*...*/

                public class Main {

                    public static void main(String[] args) {
                        
                        /*...*/
                        System.out.println(myCar);

                        /*
                        myCar.name = "Civic";
                        myCar.brand = "Honda";
                        myCar.manufactureDate = 1998;
                        System.out.println(myCar.name);
                        System.out.println(myCar.brand);
                        System.out.println(myCar.manufactureDate);
                        */
                        
                        myCar.setName("Civic");
                        myCar.setBrand("Honda");
                        myCar.setYearOfMake(1998);
                        System.out.println(myCar.getName());
                        System.out.println(myCar.getBrand());
                        System.out.println(myCar.getManufactureDate());

                        // myCar.mileage = 100_000;

                        Car otherCar = new Car("Charger", "Dodge", 1970);
                        /*
                        System.out.println(otherCar.name);
                        System.out.println(otherCar.brand);
                        System.out.println(otherCar.year_of_make);
                        */

                        System.out.println(otherCar.getName());
                        System.out.println(otherCar.getBrand());
                        System.out.println(otherCar.getManufactureDate());

                    }

                }

                /*
                Important Note:
                    - The previous approach of initializing and retrieving object properties will no longer work because the access modifier in the "Car" class has been changed to "private" meaning that the properties may only be accessed within the class which are the setters and getters.
                    - The refactored code above also makes the code more readable.
                    - This is done to ensure properties are accessed and updated/initialized only when necessary and will be discussed further in the Pillars of OOP sessions.
                */

        /*.
        4c. Add another property to demonstrate the use of getters and setters for read and write access.
            Application > src > com.zuitt > Car.java(Class)
        */

                /*...*/

                public class Car {

                    /*...*/
                    private int manufactureDate;
                    private String owner;

                    // Empty Constructor
                    public Car(){}

                    public Car(String name, String brand, int manufactureDate, String owner){
                        /*...*/
                        this.manufactureDate = manufactureDate;
                        this.owner = owner;
                    }

                    /*...*/
                    public int getManufactureDate(){
                        /*...*/
                    }
                    public String getOwner(){
                        return this.owner;
                    }

                    public void setName(String name){
                        /*...*/
                    }
                    /*...*/

                }

                /*
                Important Note:
                    - By creating a getter only, the value of the owner may only be set during initialization and cannot be updated.
                    - This is appropriate for instances where the property must not be changed.
                */

        /*
        4d. Refactor the object instantiation of the "otherCar" object.
            Application > src > com.zuitt > Main.java
        */

                /*...*/

                public class Main {

                    public static void main(String[] args) {
                        
                        /*...*/

                        Car otherCar = new Car("Charger", "Dodge", 1970, "Dominic Toretto");
                        
                        /*...*/

                        System.out.println(otherCar.getOwner());
                        otherCar.owner = "John Smith";

                    }

                }

                /*
                Important Note:
                    - Also highlight the unavailability of the "owner" property in IntelliJ's autocompletion feature since the access modifier is "private".
                    - Since the owner must not be changed, we cannot update it because the "setter" function/method for the "owner" was not created making it a read only value.
                */

/*
5. Add more code to demonstrate the use of object methods.
*/

    	/*.
        5a. Add methods for the "Car" object.
            Application > src > com.zuitt > Car.java(Class)
        */

                /*...*/

                public class Car {

                    /*...*/
                    public void setManufactureDate(int manufactureDate){
                        this.manufactureDate = manufactureDate;
                    }

                    /*
                    [Section] Methods
                    - Functions that belong to this class that allow objects to perform different functions
                    */
                    public void drive(){
                        System.out.println("The car is running");
                    }
                    public void printDetails(){
                        System.out.println("This " + getManufactureDate() + " " + getBrand() + " " + getName() + " is owned by: " + getOwner() + ".");
                    }

                }

                /*
                Important Note:
                    - By creating methods, this ensures efficiency and cleanliness of code creating functions/methods where they are needed.
                    - Since the functions "drive" and "printDetails" are functions/methods that are exclusive to the "Car" class, this is where we define them.
                */

        /*
        5b. Invoke the "Car" methods.
            Application > src > com.zuitt > Main.java
        */

                /*...*/

                public class Main {

                    public static void main(String[] args) {
                        
                        /*...*/
                        // otherCar.owner = "John Smith";
                                
                        otherCar.drive();
                        otherCar.printDetails();

                    }

                }

/*
6. Create another package and another class to demonstrate the use of access modifiers.
*/

        /*
        6a. Create another package named "accessmodifiers" and an "Example" class inside of it.
            Application > src > com.zuitt > accessmodifiers > Example.Java(Class)
        */

                /*...*/

                public class Example {

                    // [Section] Public Access Modifier
                    // The "Car" class can be accessed from the "Example" class because it has the access modifier of "public"
                    public Car defaultCar;
                    // This can be accessed in the "Main" class
                    public String message = "Hello from default";

                    // [Section] Default Access Modifier
                    // This cannot be accessed outside of the "access modifiers package"
                    String defaultMessage = "This cannot be accessed outside of the Default class";

                }

                /*
                Important Note:
                    - To create a package do the following:
                        com.zuitt + Right click > New > Package
                    - Refer to "references" section of this file to find the documentations for Java Access Modifiers and Java Access Modifiers Table.
                */

        /*
        6b. Instantiate a new "Example" class.
            Application > src > com.zuitt > Main.java
        */

            /*...*/

            public class Main {

                public static void main(String[] args) {

                    /*...*/
                    otherCar.printDetails();

                    Example accessModifier = new Example();
                    System.out.println(accessModifier.message);
                    // Since the Main class is not found inside the "accessmodifiers" package, it cannot access the "defaulyMessage" property
                    System.out.println(accessModifier.defaultMessage);

                }

            }

        /*
        6c. Create a test class to further elaborate on "default" access modifiers.
            Application > src > com.zuitt > accessmodifiers > Test.Java(Class)
        */

    		/*...*/

    	    public class Test {

                Example example = new Example();

                public void accessingDefault() {
                    System.out.println(example.defaultMessage);
                }

            }

        /*
        6d. Instantiate a new "Test" class.
            Application > src > com.zuitt > Main.java
        */

                /*...*/

                public class Main {

                    public static void main(String[] args) {

                        /*...*/
                        System.out.println(accessModifier.defaultMessage);

                        Test defaultAccess = new Test();
                        // The "defaultMessage" property can be accessed by the "Test" class which allows us to print the message in the "Main" class
                        defaultAccess.accessingDefault();

                    }

                }

        /*
        6e. Add a "protected" property in the "Example" class.
            Application > src > com.zuitt > accessmodifiers > Example.Java(Class)
        */

                /*...*/

                public class Example {
                    
                    /*...*/
                    String defaultMessage = "This cannot be accessed outside of the Default class";

                    // [Section] Protected Access Modifier
                    // Can only be accessed inside the same package and outside the package through a child/sub class
                    protected String protectedMessage = "This is a protected message";

                }

        /*
        6f. Create another class to demonstrate the use of protected access modifiers.
            Application > src > com.zuitt > Main.java
        */

                /*...*/

                public class Main {

                    public static void main(String[] args) {

                        /*...*/
                        defaultAccess.accessingDefault();

                        Protected protectedAccess = new Protected();
                        protectedAccess.printProtectedMessage();

                    }

                    // The "extends" keyword allows to create a parent-child/sub class relationship between the "Example" and "Protected" class
                    public static class Protected extends Example {
                        public void printProtectedMessage() {
                            System.out.println(protectedMessage);
                        }
                    }

                }

                /*
                Important Note:
                    - The "extends" keyword will be explained further in the Pillars of OOP sessions.
                */

        /*
        6g. Create another class to demonstrate the use of protected access modifiers.
            Application > src > com.zuitt > accessmodifiers > Example.Java(Class)
        */

                /*...*/

                public class Example {
                    
                    /*...*/
                    protected String protectedMessage = "This is a protected message";

                    // [Section] Private Access Modifier
                    // Can only be accessed within the same class
                    private String privateMessage = "This message is private";

                    public void printPrivateMessage() {
                        System.out.println(privateMessage);
                    }

                }

        /*
        6h. Add code to demonstrate the use of private access modifiers.
            Application > src > com.zuitt > Main.java
        */

                /*...*/

                public class Main {

                    public static void main(String[] args) {

                        /*...*/
                        // System.out.println(accessModifier.defaultMessage);
                        
                        // The "privateMessage" property can only be accessed within it's class
                        System.out.println(accessModifier.privateMessage);
                        // The message can only be printed out because the "printPrivateMessage" method has access to the "privateMessage" class
                        accessModifier.printPrivateMessage();

                        Test defaultAccess = new Test();
                        /*...*/

                    }

                    /*...*/

                }

/*
========
Activity
========
*/

/*
1. Create a new Java project named "activity".
    Documents > PNT006 > S05 > activity
*/

/*
2. Create a "User" class.
    Application > src > com.zuitt > User.java(Class)
*/

    	/*...*/

        public class User {

            // Properties
            private String firstName;
            private String lastName;
            private String email;
            private String contactNumber;

            // Constructors
            public User(){}

            public User(String firstName, String lastName, String email, String contactNumber, String id) {
                this.firstName = firstName;
                this.lastName = lastName;
                this.email = email;
                this.contactNumber = contactNumber;
                this.id = id;
            }

            // Getters and Setters
            public String getFirstName() {
                return this.firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getLastName() {
                return this.lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getEmail() {
                return this.email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getContactNumber() {
                return this.contactNumber;
            }

            public void setContactNumber(String contactNumber) {
                this.contactNumber = contactNumber;
            }

            // Methods
            public void introduce(){
                System.out.println("Hello my name is " + getFirstName() + " " + getLastName() + ".");
            }

            public void provideContact(){
                System.out.println("You may contact me at " + getContactNumber() + " or you may email me at " + getEmail());
            }

        }

/*
3. Instantiate two user objects and invoke the methods.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class Main {

            public static void main(String[] args) {

                User firstUser = new User();
                firstUser.setFirstName("John");
                firstUser.setLastName("Smith");
                firstUser.setContactNumber("09123456789");
                firstUser.setEmail("john@mail.com");

                firstUser.introduce();
                firstUser.provideContact();

                User secondUser = new User("Jane", "Smith", "0987654321", "jane@mail.com", "1234");

                secondUser.introduce();
                secondUser.provideContact();

            }
        }

/*
4. Add an "id" property and a getter function for it.
    Application > src > com.zuitt > Main.java
*/

        /*...*/

        public class User {

            /*...*/
            private String contactNumber;
            private String id;

            public User(){}

            /*...*/

            public void setContactNumber(String contactNumber) {
                this.contactNumber = contactNumber;
            }

            public String getId() {
                return this.id;
            }

            public void introduce(){
                /*...*/
            }

            /*...*/

        }

/*
5. Add a "Scanner" class to input the "firstUser" details instead of hard coded values.
    Application > src > com.zuitt > Main.java
*/

    	/*...*/

        public class Main {

            public static void main(String[] args) {

                User firstUser = new User();
                /*
                firstUser.setFirstName("John");
                firstUser.setLastName("Smith");
                firstUser.setContactNumber("09123456789");
                firstUser.setEmail("john@mail.com");
                */

                // Stretch Goal
                Scanner appScanner = new Scanner(System.in);
                System.out.println("What's your first name?");
                firstUser.setFirstName(appScanner.nextLine());
                System.out.println("What's your last name?");
                firstUser.setLastName(appScanner.nextLine());
                System.out.println("What's your contact number?");
                firstUser.setContactNumber(appScanner.nextLine());
                System.out.println("What's your email address?");
                firstUser.setEmail(appScanner.nextLine());

                firstUser.introduce();
                /*...*/

            }

        }