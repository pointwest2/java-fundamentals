package com.zuitt;

public class User {

    // Properties
    private String firstName;
    private String lastName;
    private String email;
    private String contactNumber;
    private String id;

    // Constructors
    public User(){}

    public User(String firstName, String lastName, String email, String contactNumber, String id) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.contactNumber = contactNumber;
        this.id = id;
    }

    // Getters and Setters
    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return this.contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getId() {
        return this.id;
    }

    // Methods
    public void introduce(){
        System.out.println("Hello my name is " + getFirstName() + " " + getLastName() + ".");
    }

    public void provideContact(){
        System.out.println("You may contact me at " + getContactNumber() + " or you may email me at " + getEmail());
    }

}
