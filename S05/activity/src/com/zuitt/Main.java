package com.zuitt;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        User firstUser = new User();
        /*
        firstUser.setFirstName("John");
        firstUser.setLastName("Smith");
        firstUser.setContactNumber("09123456789");
        firstUser.setEmail("john@mail.com");
        */

        // Stretch Goal
        Scanner appScanner = new Scanner(System.in);
        System.out.println("What's your first name?");
        firstUser.setFirstName(appScanner.nextLine());
        System.out.println("What's your last name?");
        firstUser.setLastName(appScanner.nextLine());
        System.out.println("What's your contact number?");
        firstUser.setContactNumber(appScanner.nextLine());
        System.out.println("What's your email address?");
        firstUser.setEmail(appScanner.nextLine());

        firstUser.introduce();
        firstUser.provideContact();

        User secondUser = new User("Jane", "Smith", "jane@mail.com", "0987654321", "1234");

        secondUser.introduce();
        secondUser.provideContact();

    }
}
