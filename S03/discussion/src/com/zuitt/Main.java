package com.zuitt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        /*
        [Section] Conditional statements
        - Helps control the flow of a program
        - The curly braces ({}) are also optional if only a single statement will be executed and must be used in conjunction with proper indentation to ensure code readability
        - For multiple statement execution, using curly braces will would be required and is better for code readability
        */
        int num1 = 10;
        int num2 = 20;

        // If Statements
        if (num1 > 5)
            System.out.println("Num1 is greater than 5");

        // If-Else Statements
        if (num2 > 100)
            System.out.println("Num2 is greater than 100");
        else
            System.out.println("Num2 is less than 100");

        // If-ElseIf-Else Statements
        if (num1 == 5)
            System.out.println("Num1 is equal to 5");
        else if (num2 == 20)
            System.out.println("Num2 is equal to 20");
        else
            System.out.println("Anything Else");


        // Using boolean values with conditional statements
        // When boolean values are used, you may omit the equality operators
        boolean isHandsome = true;

        if(isHandsome)
            System.out.println("Hello handsome");

        // [Section] Logical and(&&)/or(||)/not(!) Operators
        boolean isMarried = false;
        boolean isRetired = true;

        // All expressions must be true
        if (isMarried == false && isRetired == false)
            System.out.println("You are a devote member of society.");

        // At least one expression is true
        if (isMarried || isRetired)
            System.out.println("Thank you for your service!");

        // Reverses the boolean value
        if (!isMarried && isRetired)
            System.out.println("You have lived a wonderful life.");

        /*
        [Section] String comparison
        - In java, comparing to strings can be done using the "equals" method instead of using the equality operator like in other languages
        */
        String word = "hello";

        // if (word == "hello"){
        //    System.out.println("hi");
        // }
        if (word.equals("hello")){
            System.out.println("hi");
        }

        // isBlank method
        // Checks if the string is blank or contains only white space
        String blank = " ";
        System.out.println("Result of isBlank Method:");
        System.out.println(blank.isBlank());

        // isEmpty method
        // Checks ONLY if the string has a length of 0
        String empty = " ";
        System.out.println("Result of isEmpty Method:");
        System.out.println(empty.length());
        System.out.println(empty.isEmpty());

        /*
        [Section] Short Circuiting
        - When using logical operators in Java, when an expression is found to be true, Java will short circuit and will not continue to evaluate other expressions
        - The code may seem correct and will still function, however errors will not be captured until encountered which will cause problems during development
        */
        int x = 15;
        int y = 0;

        // This well return an error
        // System.out.println(x/y == 0);

        // The second condition "x/y == 0" will never return an error because of short circuiting
        // Even though the error produced by "x/y" will return an error and was used twice, the application will still run as intended
        // "y > 5" will always be false given our example, thus "x/y == 0" will not be evaluated and the code block inside will never be run
        if (y > 5 && x/y == 0)
            System.out.println("Result is: " + x/y);
        else
            System.out.println("The condition has short circuited");

        // [Section] Switch Statement
        Scanner appScanner = new Scanner(System.in);

        /*
        System.out.println("Provide a number between 1-4:");
        int directionValue = appScanner.nextInt();
        System.out.println("Result of Switch Statements");
        switch (directionValue) {
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("Invalid");
                break;
        }
        */

        // Enhanced Switch Statement (Java 13)
        // Avoids problems when "break" statement is forgotten
        /*
        switch (directionValue) {
            case 1 -> System.out.println("North");
            case 2 -> System.out.println("South");
            case 3 -> System.out.println("East");
            case 4 -> System.out.println("West");
            default -> System.out.println("Invalid");
        }
        */

        // Allows for multiple values per case
        /*
        System.out.println("Choose a number between 1-5:");
        int option = appScanner.nextInt();

        switch (option) {
            case 1, 2 -> System.out.println("The option is >3");
            case 4, 5 -> System.out.println("The option is <3");
            default -> System.out.println("The option is 3");
        }
        */

        // Allows use of switch statement as an expression and return values via the "yield" statement
        /*
        System.out.println("Choose an option between 1 and 2:");
        int choice = appScanner.nextInt();

        String response = switch (choice) {
            case 1:
                yield "true";
            case 2:
                yield "false";
            default:
                yield "Incorrect response";
        };

        System.out.println(response);
        */

        // [Section] Loops
        // For loops
        for (int i = 0; i <= 5; i++) {
            System.out.println("Current count: " + i);
        }

        // For loops with arrays
        int[] intArray = {100, 200, 300, 400, 500};
        for (int i = 0; i < intArray.length; i++) {
            System.out.println(intArray[i]);
        }

        // ForEach loops with arrays
        String[] nameArray = {"John", "Paul", "George"};
        // for(dataType parameterName : arrayName){
        //      code to execute
        // }
        for(String name : nameArray){
            System.out.println(name);
        }

        // ForEach loops with ArrayLists
        ArrayList<String> disneyCharacters = new ArrayList<>(Arrays.asList("Mickey" , "Donald", "Goofy"));

        for(String character : disneyCharacters){
            System.out.println(character);
        }

        // Looping through Multidimensional Arrays
        String[][] classroom = new String[3][3];

        // First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        // Second row
        classroom[1][0] = "Mickey";
        classroom[1][1] = "Donald";
        classroom[1][2] = "Goofy";

        // Third row
        classroom[2][0] = "Harry";
        classroom[2][1] = "Ron";
        classroom[2][2] = "Hermione";

        for(int row = 0; row < 3; row++){
            for(int col = 0; col < 3; col++){
                System.out.println(classroom[row][col]);
            }
        }

        // While loop
        int a = 1;

        while (a < 5){
            System.out.println("Loop Number " + a);
            a++;
        }

        // Do While loop
        int b = 0;

        do {
            System.out.println("Countdown " + b);
            b--;
        } while (b > 10);

        // While loop with user input
        String name = "";

        while(name.isBlank()){
            System.out.println("What's your name? Type goodbye to exit.");
            name = appScanner.nextLine();

            if (name.equalsIgnoreCase("Goodbye")){
                System.out.println("See you later!");
            } else if (!name.isBlank()){
                System.out.println("Hi " + name);
            }
        }

    }

}
