package com.zuitt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        System.out.println("Input year to be checked if a leap year:");
        int year = appScanner.nextInt();

        // A leap year is divisible by 4 but NOT divisible by 100 UNLESS it is ALSO divisible by 400
        // If year / 4 = 0 -> if year / 100 = 0 -> if year/400 -> leap year
        //      |                   |                   |
        //      v                   v                   v
        // not a leap year      leap year           not a leap year
        if(year % 4 == 0){

            if(year % 100 == 0){

                if(year % 400 == 0){

                    System.out.println(year + " is a leap year");

                } else {

                    System.out.println(year + " is NOT a leap year");

                }

            } else {

                System.out.println(year + " is a leap year");
            }

        } else {

            System.out.println(year + " is NOT a leap year");

        }

        ArrayList<String> users = new ArrayList<>(Arrays.asList("Mickey", "Donald", "Goofy", "Minnie", "Daisy"));
        ArrayList<String> filteredUsers = new ArrayList<>();
        String searchTerm = "";

        System.out.println("Provide a string to filter current users:");
        System.out.println(users);

        while(searchTerm.isBlank()){

            searchTerm = appScanner.nextLine().toLowerCase().trim();

            if (!searchTerm.isBlank()) {

                for (String user : users) {
                    if (user.contains(searchTerm)) {
                        filteredUsers.add(user);
                    }
                }

                System.out.println("The following users were found:");
                System.out.println(filteredUsers);

            }

        }

    }

}
