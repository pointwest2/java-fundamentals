package com.zuitt;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // Variables to store the user's data
        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;

        // Instantiate a new "Scanner" object
        Scanner appScanner = new Scanner(System.in);

        // Print a message in the console and retrieve the user's input using the scanner methods
        System.out.println("First Name:");
        firstName = appScanner.nextLine().trim();
        System.out.println("Last Name:");
        lastName = appScanner.nextLine().trim();
        System.out.println("First Subject Grade:");
        firstSubject = appScanner.nextDouble();
        System.out.println("Second Subject Grade:");
        secondSubject = appScanner.nextDouble();
        System.out.println("Third Subject Grade:");
        thirdSubject = appScanner.nextDouble();

        // Get the average of the user's grades
        int average = ((int) firstSubject + (int) secondSubject + (int) thirdSubject) / 3;

        System.out.println("Good day, " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + average);

    }
}
