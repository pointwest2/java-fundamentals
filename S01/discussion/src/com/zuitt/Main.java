/*
[Section] Packages and Classes
- Classes are "blueprints" for creating objects and Java is known to be an Object-Oriented Programming language
- A package in Java is used to "group related classes" similar to how a folder is related to it's files
- Java package where the "main" class/file belongs to
*/
package com.zuitt;

import java.util.Scanner;

/*
[Section] Main Class
- The "Main" class is the entry point of a Java program and is responsible for executing our code
- Every Java program should have at least one "Main" class and one "main" function inside it
*/
public class Main {

    /*
   [Section] main Function
   - This is where most of the executable code is applied to
   - Syntax
       public static void functionName(dataType argumentName) {
           // code to be executed
       }
   - The "public" is an access modifier which tells our application what parts of the program can access the main function
   - The "void" is the return statement's data type of the "main" function that defines what kind of data it will return. Since there is no return statement and the main function returns nothing then the return data type is voided/is empty
   */
    public static void main(String[] args) {
        System.out.println("Hello World");

        /*
        [Section] Variables
        - Variables in Java require the dataType of the value to be specified
        - The variable names are called "identifiers" which is commonly referenced to when troubleshooting
        - Variables may be declared without an initial value
        - Syntax
            Variable declaration
                dataType identifier
            Variable declaration and initialization
                dataType identifier = value;
        */

        // Variable Declaration
        // Variable is created but not assigned a value
        int myNum;
        // System.out.println(myNum);

        // Variable Declaration and Initialization
        // Variable is created and assigned an initial value
        int mySecondNum = 29;
        System.out.println("Result of variable declaration and initialization:");
        System.out.println(mySecondNum);

        // Initialization after declaration
        // The first instance that a variable is given a valye
        myNum = 1;
        System.out.println("Result of variable initialization after declaration:");
        System.out.println(myNum);

        // Variable re-assignment;
        // The intial value of the variable was already assigned and is changed by providing a different value
        mySecondNum = 30;
        System.out.println("Result of variable re-assignment:");
        System.out.println(mySecondNum);

        // Constants
        // Naming convention for constants in Java is to use all uppercase letters
        final int PRINCIPAL = 1000;
        // PRINCIPAL = 500;

        /*
        [Section] var Keyword and Local Scope
        - Automatic implication of the data type
        - Has a local scope meaning it's only available to within the function it was declared in
        */
        var unknown = "Hello";
        System.out.println(unknown);

        /*
        [Section] Primitive Data Types
        - Used to store simple values
        - For number values, each primitive data type can only store up to a certain value to maximize memory consumption
        */

        // Single quotes are used for characters
        char letter = 'A';
        boolean isMarried = false;

        byte students = 127;
        // byte population = 128;
        short seats = 32767;
        // Underscores may be placed in between numbers for code readability
        int localPopulation = 2_147_483_647;
        System.out.println("The current local population is: " + localPopulation);
        // Java recognizes whole numbers as integers and using the "L" suffix denotes a long data type
        long worldPopulation = 7_862_081_145L;

        /*
        [Section] Floats and Doubles
        - Java recognizes decimal numbers as doubles and using the "F" suffix denotes a Floating data type
        - The difference between using float and double depends on the preciseness of the values.
        - Doubles have access to more decimal places making it more accurate.
        */
        float price = 12.99F;
        double temperature = 15683.8623941;

        // The "Object" class allows us to convert primitive data types into objects giving us access to the "getClass" method for checking the data type of a value
        System.out.println("Result of getClass Method:");
        System.out.println(((Object)temperature).getClass());

        /*
        [Section] Non-Primitive / Referencing Data Types
        - Used to store complex data/objects
        - Has access to methods for manipulating data
        */
        // String name = new String("John Doe");
        String name = "John Doe";
        System.out.println("Result of Non-Primitive Data Types:");
        System.out.println(name);
        String editedName = name.toLowerCase();
        System.out.println(editedName);

        // Escape sequence
        System.out.println("Result of escape sequence:");
        System.out.println("c:\\windows\\desktop");

        System.out.println("Result of getClass Method:");
        System.out.println(name.getClass());

        /*
        [Section] Arithmetic Expressions
        */
        int result = 10 + 5;
        System.out.println(result);

        /*
        [Section] Casting / Type Casting
        - Implicit Casting refers to the automatic conversion of a  smaller number to a larger number
        - Explicit Casting refers to the definition of conversion from a larger number into a smaller number
        - The sequence of conversion follows this sequence:
            byte > short > int > long > float > double
        - Syntax for explicit casting
            (dataType) value/variable;
        */

        // Implicit Casting
        int num1 = 5;
        double num2 = 2.7;
        double total = num1 + num2;
        System.out.println("Result from implicit casting:");
        System.out.println(total);

        // Explicit Casting
        int num3 = 5;
        double num4 = 2.7;
        int anotherTotal = num3 + (int)num4;
        System.out.println("Result from explicit casting:");
        System.out.println(anotherTotal);

        // Converting strings to integers
        // Most form data is received as a string
        String mathGrade = "90";
        String englishGrade = "85";
        System.out.println("Total grade is: " + mathGrade + englishGrade);

        int totalGrade = Integer.parseInt(mathGrade) + Integer.parseInt(englishGrade);
        System.out.println("Total grade is: " + totalGrade);
        String stringGrade = Integer.toString(totalGrade);
        System.out.println(stringGrade.getClass());

        /*
        [Section] Scanner Class
        - Used to get input from the user
        - Creates a "Scanner" object that allows access to multiple methods for retrieving user input in the console
        - Syntax
            Scanner scannerName = new Scanner(System.in);
            scannerName.nextLine();
            scannerName.nextInt();
        */
        // Variables to store the user's input
        String myName;
        int myAge;

        // Instantiate a new scanner object
        Scanner appScanner = new Scanner(System.in);

        // The ".nextLine()" method of the scanner class will accept the whole next line of the user's input
        // The ".trim()" method can be chained to the ".nextLine()" method to remove spaces before and after the user's input.
        System.out.println("What's your name?");
        myName = appScanner.nextLine().trim();
        // The ".nextInt()" method will accept the next integer of the user's input
        System.out.println("What's your age?");
        myAge = appScanner.nextInt();
        System.out.println("My name is " + myName + ". I'm " + myAge + " years old.");

    }

    public void hello() {
        // "unknown" and "myNum" are not recognized because they are declared in the "main" function and not in the "hello" function
        // System.out.println(unknown);
        // System.out.println(myNum);
    }

}
