import java.util.ArrayList;

public class EmployeeController {

    // serves as the database to store employees
    private ArrayList<Employee> employees = new ArrayList<>();

    public String createEmployee(String firstName, String lastName, int age, String contactNumber) {

        String message;

        if(firstName.trim().isEmpty() || lastName.trim().isEmpty() || age == 0 || contactNumber.trim().isEmpty()){
            message = "Please fill in all the details";
        } else {
            Employee newEmployee = new Employee(firstName, lastName, age, contactNumber);
            employees.add(newEmployee);
            message = "New employee added";
        }

        return message;

    }

    public String getEmployeesLastNames() {

        ArrayList<String> employeesLastNames = new ArrayList<>();

        for(Employee employee: employees) {
            employeesLastNames.add(employee.getLastName());
        }

        return employeesLastNames.toString();
    }

    public int getIndexByFirstName(String firstName) {

        int employeeIndex = -1;

        for(Employee employee: employees) {
            if (employee.getFirstName().equalsIgnoreCase(firstName)) {
                employeeIndex = employees.indexOf(employee);
            }
        }

        return employeeIndex;
    }

    public String updateEmployee(String employeeFirstName, Employee updatedEmployee) {
        int employeeIndex = getIndexByFirstName(employeeFirstName);
        String message;

        if(employeeIndex >= 0) {
            employees.set(employeeIndex, updatedEmployee);
            message = "Employee updated successfully";
        } else {
            message = "Employee not found";
        }

        return message;

    }

    public String deleteEmployee(String employeeFirstName) {
        int employeeIndex = getIndexByFirstName(employeeFirstName);
        String message;

        if(employeeIndex >= 0) {
            employees.remove(employeeIndex);
            message = "Employee deleted successfully";
        } else {
            message = "Employee not found";
        }

        return message;

    }

}
