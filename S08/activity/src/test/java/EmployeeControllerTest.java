import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EmployeeControllerTest {

    EmployeeController employeeController;
    Employee updatedEmployee;

    @BeforeEach
    void executeEachTest() {
        employeeController = new EmployeeController();
        employeeController.createEmployee("John", "Doe", 29, "09123456789");
        updatedEmployee = new Employee("Jane", "Doe", 21, "09987654321");
    }

    @Test
    @DisplayName("Return an error message if employee details are blank creating an employee.")
    public void givenEmployeeDetails_returnsCreateErrorStatusMessage() {
        String actualResult = employeeController.createEmployee(" ", "Doe", 29, "09123456789");
        String expectedResult = "Please fill in all the details";
        assertEquals(expectedResult, actualResult);
    }

    @Test
    @DisplayName("Return success message after successfully creating an employee.")
    public void givenEmployeeDetails_returnsCreateSuccessStatusMessage() {
        String actualResult = employeeController.createEmployee("John", "Doe", 29, "09123456789");
        String expectedResult = "New employee added";
        assertEquals(expectedResult, actualResult);
    }

    @Test
    @DisplayName("Return the last names of employees as a string.")
    public void givenNone_returnsStringOfEmployeesLastNames() {
        String actualResult = employeeController.getEmployeesLastNames();
        String expectedResult = "[Doe]";
        assertEquals(expectedResult, actualResult);
    }

    @Test
    @DisplayName("Return the index of an employee from the array list based on the first name.")
    public void givenEmployeeFirstName_returnsIndexOfEmployee() {
        int actualResult = employeeController.getIndexByFirstName("John");
        int expectedResult = 0;
        assertEquals(expectedResult, actualResult);
    }

    @Test
    @DisplayName("Update an employee based on their first name and return success message.")
    public void givenEmployeeFirstName_returnsUpdateSuccessMessage() {
        String actualResult = employeeController.updateEmployee("John", updatedEmployee);
        String expectedResult = "Employee updated successfully";
        assertEquals(expectedResult, actualResult);
    }

    @Test
    @DisplayName("Update an employee based on their first name and return error message.")
    public void givenEmployeeFirstName_returnsUpdateErrorMessage() {
        String actualResult = employeeController.updateEmployee("Joe", updatedEmployee);
        String expectedResult = "Employee not found";
        assertEquals(expectedResult, actualResult);
    }

    @Test
    @DisplayName("Delete an employee based on their first name and return success message.")
    public void givenEmployeeFirstName_returnsDeleteSuccessMessage() {
        String actualResult = employeeController.deleteEmployee("John");
        String expectedResult = "Employee deleted successfully";
        assertEquals(expectedResult, actualResult);
    }

    @Test
    @DisplayName("Delete an employee based on their first name and return error message.")
    public void givenEmployeeFirstName_returnsDeleteErrorMessage() {
        String actualResult = employeeController.deleteEmployee("Joe");
        String expectedResult = "Employee not found";
        assertEquals(expectedResult, actualResult);
    }

}
