/*
======================================
PNT006 - S08 - Unit Testing with JUnit
======================================
*/

/*

References
    JUnit5 vs JUnit4
        https://howtodoinjava.com/junit5/junit-5-vs-junit-4/
    JUnit 5 Tutorial
        https://www.vogella.com/tutorials/JUnit/article.html
    Java @Before, @BeforeAll, @BeforeEach and @BeforeAll Annotations
        https://www.baeldung.com/junit-before-beforeclass-beforeeach-beforeall
    @Before/@BeforeEach Method Naming Convention
        https://stackoverflow.com/questions/66094564/what-is-the-convention-for-beforeclass-beforeall-and-afterclass-afterall
    Naming standards for unit tests
        https://osherove.com/blog/2005/4/3/naming-standards-for-unit-tests.html
    JUnit @DisplayName Annotation
        https://junit.org/junit5/docs/5.0.3/api/org/junit/jupiter/api/DisplayName.html
    JUnit Best Practices Guide
        https://howtodoinjava.com/best-practices/unit-testing-best-practices-junit-reference-guide/
    JUnit assertEquals Method
        http://junit.sourceforge.net/javadoc/org/junit/Assert.html
    Best Practices for Unit Testing in Java
        https://www.baeldung.com/java-unit-testing-best-practices
    JUnit @DisplayNameGeneration Annotation
        https://junit.org/junit5/docs/5.6.0/api/org.junit.jupiter.api/org/junit/jupiter/api/DisplayNameGeneration.html
    JUnit @DisplayNameGenerator Interface
        https://junit.org/junit5/docs/5.6.0/api/org.junit.jupiter.api/org/junit/jupiter/api/DisplayNameGenerator.html
    
		

Definition of terms
    Application - Root folder for the application

*/

/*
==========
Discussion
==========
*/

/* 
1. Create a new Java project with Maven named "discussion".
    Documents > VRT001 > S09 > discussion
*/

        /*
        Important Note:
            - Refer to the Google slide for this session on how to create a project with Maven.
        */

/* 
2. Add the dependencies and download the resources.
    Application > pom.xml
*/

        /*...*/
        <project xmlns="http://maven.apache.org/POM/4.0.0"
                 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
            /*...*/
            <version>1.0-SNAPSHOT</version>

            <dependencies>
                <dependency>
                    <groupId>org.junit.jupiter</groupId>
                    <artifactId>junit-jupiter</artifactId>
                    <version>RELEASE</version>
                    <scope>test</scope>
                </dependency>
                <dependency>
                    <groupId>org.junit.jupiter</groupId>
                    <artifactId>junit-jupiter</artifactId>
                    <version>RELEASE</version>
                    <scope>test</scope>
                </dependency>
            </dependencies>

            <properties>
                /*...*/
            </properties>

        </project>

        /*
        Important Note:
            - Maven will be used in this session to be able to create unit tests with JUnit and will be further discussed in the java spring boot sessions.
            - To download the dependencies right click on the pom.xml >maven > download sources.
            - A bar will display on the lower right hand side of IntelliJ environment indicating the completion of the download.
            - After the download has completed right click on the pom.xml > maven > reload project.
            - After reloading the project the JUnit packages can be found under the "external libraries" portion of the project structure.
            - Alternatively, you can skip adding the dependencies and just add the JUnit annotations and intelliJ will be able to detect the package needed and download it as needed. 
        */

/* 
3. Create an "Employee" class with a simple method to use for the unit testing.
    Application > src > main > java > Employee.java (Class)
*/

        public class Employee {

            private String firstName;
            private String lastName;
            private String department;
            private String hireDate;

            public Employee() {
            }

            public Employee(String firstName, String lastName, String department, String hireDate) {
                this.firstName = firstName;
                this.lastName = lastName;
                this.department = department;
                this.hireDate = hireDate;
            }

            public String printFullName() {
                return firstName + " " + lastName;
            }

        }

/* 
4. Create an "EmployeeTest" class.
    Application > src > test > java > EmployeeTest.java (Class)
*/

        /*...*/

        /*
        * Naming convention for Test classes is the name of the class and the suffix of
        * "Test" for code readability
        */
        public class EmployeeTest {

            // Property to be used for all test cases
            Employee employee;

            /*
            * The "@BeforeEach" annotation allows execution of code before running a test
            * This is best used for common code execution
            * Runs before each test to instantiate an employee object
            * The "@Before" annotation was used for JUnit 4
            * "setUp" was a required naming convention for methods annotated with
            * @Before using JUnit 3. As of JUnit 4 and onwards, using meaningful names
            * or names based on the annotation are acceptable
            */
            @BeforeEach
            void instantiateEmployee() {
                employee = new Employee("John", "Doe", "Engineering", "2021-10-24");
            }

        }

        /*
        Important Note:
            - Refer to "references" section of this file to find the documentations for JUnit5 vs JUnit4, JUnit 5 Tutorial, Java @Before, @BeforeAll, @BeforeEach and @BeforeAll Annotations and @Before/@BeforeEach Method Naming Convention.
        */

/* 
5. Add methods to demonstrate the use of the "@BeforeAll" and "@AfterAll" annotations.
    Application > src > test > java > EmployeeTest.java (Class)
*/

        /*...*/

        public class EmployeeTest {

            /*...*/
            @BeforeEach
            void instantiateEmployee() {
                /*...*/
            }

            /*
            * The "@BeforeAll" functions similarly as the "@BeforeEach" annotation but
            * is preferably used for common expensive operations to be executed once
            * before running all tests such as connecting to a database.
            * "setUp" was a required naming convention for methods annotated with
            * "@Before" using JUnit 3. As of JUnit 4 and onwards, using meaningful names
            * or names based on the annotation are acceptable.
            * The "@BeforeClass" annotation was used for JUnit 4.
            */
            @BeforeAll
            public static void setUp() {
                System.out.println("Connected to database.");
            }

            /*
            * The "@AfterAll" annotation executes code after each test and was a
            * required naming convention as of JUnit 3.
            */
            @AfterAll
            public static void tearDown() {
                System.out.println("Closing database connection.");
            }

        }

/* 
6. Create a unit test for the "printFullName" method and run the test.
    Application > src > test > java > EmployeeTest.java (Class)
*/

        /*...*/

        public class EmployeeTest {

            /*...*/
            public static void tearDown() {
                /*...*/
            }

            // A method annotated with @Test defines a test method
            @Test
            /*
            * The "@DisplayName" annotation can be used to declare a custom display name
            * for code readability.
            */
            @DisplayName("Print the employee's first and last name as a single string.")
            /*
            * One naming convention for test methods can be in the pattern of:
            * [UnitOfWork_StateUnderTest_ExpectedBehavior]
            */
            public void printFullName_employeeProperties_returnsStringFullName() {
                /* 
                * Expected vs. Actual
                * A test case should have an assertion between expected and actual values.
                * It's suggested to prefix the variable names with the words "actual"
                * and "expected" keywords to improve code readability
                */
                String actualResult = employee.printFullName();
                String expectedResult = "John Doe";
                assertEquals(expectedResult, actualResult);
            }

        }

        /*
        Important Note:
            - Right click the "EmployeeTest.java" class and select the "run EmployeeTest" option.
            - Refer to "references" section of this file to find the documentations for JUnit @DisplayName Annotation, JUnit Best Practices Guide and JUnit assertEquals Method. 
        */

/* 
7. Add another method to use for the unit testing.
    Application > src > main > java > Employee.java (Class)
*/

        /*...*/

        public class Employee {

            /*...*/

            public String printFullName() {
                /*...*/
            }

            // Method to store data in a database
            public String create(String firstName, String lastName, String department, String hireDate) {

                String result;

                if (firstName.trim().length() == 0 || lastName.trim().length() == 0 || department.trim().length() == 0 || hireDate.trim().length() == 0) {
                    result = "The first name, last name, department and hireDate should not be empty.";
                }

                return result;
            }

        }

/* 
8. Create another unit test.
    Application > src > test > java > EmployeeTest.java (Class)
*/

        /*...*/

        public class EmployeeTest {

            /*...*/
            public void printFullName_employeeProperties_returnsStringFullName() {
                /*...*/
            }

            @Test
            @DisplayName("Ensure all properties are not empty.")
            public void givenEmployeeDetails_returnsCreateErrorMessage() {
                String actualResult = employee.create("Jane", "Doe", "HR", " ");
                String expectedResult = "The first name, last name, department and hireDate should not be empty.";
                assertEquals(expectedResult, actualResult);
            }

        }

        /*
        Important Note:
            - For majority of the annotations, hovering over the annotation will provide an option to "import class".
            - For the "@Test" annotation upon clicking the "import class" select the package for "org.junit.jupiter"
            - For the "assertEquals" method hover over the method and select "more actions" > "import static method" > "org.junit.jupiter..."
            - Refer to "references" section of this file to find the documentations for JUnit 5 Tutorial and Best Practices for Unit Testing in Java.
        */

/* 
9. Modify the "create" method to return a success message.
    Application > src > main > java > Employee.java (Class)
*/

        /*...*/

        public class Employee {

            /*...*/

            public String create(String firstName, String lastName, String department, String hireDate) {

                /*...*/

                if (firstName.trim().length() == 0 || lastName.trim().length() == 0 || department.trim().length() == 0 || hireDate.trim().length() == 0) {
                    /*...*/
                } else {
                    result = "Employee successfully created.";
                }

                return result;
            }

        }

/* 
10. Add another unit test for the "create" method to capture all possible outcomes.
    Application > src > test > java > EmployeeTest.java (Class)
*/

        /*...*/

        public class EmployeeTest {
            
            /*...*/

            @Test
            @DisplayName("Ensure all properties are not empty.")
            public void givenEmployeeDetails_returnsCreateErrorMessage() {
                /*...*/
            }

            /*
            * It is best practice to create unit tests for all possible outcomes to 
            * capture any errors that may be encountered.
            * Alternating between code modification and unit testing is a good way to
            * ensure that all results are accounted for.
            */
            @Test
            @DisplayName("Successful employee creation.")
            public void givenEmployeeDetails_returnsCreateSuccessMessage() {
                String actualResult = employee.create("Jane", "Doe", "HR", "2021-05-01");
                String expectedResult = "Employee successfully created.";
                assertEquals(expectedResult, actualResult);
            }

        }

/* 
11. Add the "@DisplayNameGeneration" annotation and comment out the "@DisplayName" annotation from the latest unit test.
    Application > src > test > java > EmployeeTest.java (Class)
*/

        /*...*/

        /*
        * The "@DisplayNameGeneration" annotation allows custom display names for the
        * whole class removing repetitive "@DisplayName" annotations.
        * This is best used with certain naming conventions to make the display names be more readable.
        */
        @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
        public class EmployeeTest {

            /*...*/

            @Test
            // @DisplayName("Successful employee creation.")
            public void givenEmployeeDetails_returnsCreateSuccessMessage() {
                /*...*/
            }

        }

        /*
        Important Note:
            - Refer to "references" section of this file to find the documentations for JUnit @DisplayNameGeneration Annotation and JUnit @DisplayNameGenerator Interface.
        */

/* 
12. Add an "isAdmin" property and getter for the "Employee" class.
    Application > src > main > java > Employee.java (Class)
*/

        /*...*/

        public class Employee {

            /*...*/
            private String hireDate;
            private boolean isAdmin;

            public Employee() {
            }

            public Employee(String firstName, String lastName, String department, String hireDate) {
                /*...*/
            }
            
            public boolean isAdmin() {
                return isAdmin;
            }
            
            public String printFullName() {
                /*...*/
            }

            /*...*/

        }

/* 
13. Add another unit test for the "isAdmin".
    Application > src > test > java > EmployeeTest.java (Class)
*/

        /*...*/

        public class EmployeeTest {
            
            /*...*/
        public void givenEmployeeDetails_returnsCreateSuccessMessage() {
                /*...*/
            }

            @Test
            public void givenNone_returnBooleanAdminStatusFalse() {
                boolean actualResult = employee.isAdmin();
                assertFalse(actualResult);
            }

        }

/* 
11. Add a "getSalaryGrade" method in the "Employee" class to be used in the unit test.
    Application > src > main > java > Employee.java (Class)
*/

        public class Employee {

            /*...*/

            public String create(String firstName, String lastName, String department, String hireDate) {
                /*...*/
            }

            public int getSalaryGrade(int salary) {
                // Test salary range and return salary grade
                if (salary < 10000) {
                    throw new IllegalArgumentException("Salary must be greater or equal to 10000.");
                } else {
                    if (salary >= 10000 && salary <= 20000) {
                        return 1;
                    }
                    else if (salary > 20000 && salary <= 30000) {
                        return 2;
                    }
                    else {
                        return 3;
                    }
                }
            }

        }

/* 
12. Add a unit tests for the "getSalaryGrade" method to capture an exception.
    Application > src > test > java > EmployeeTest.java (Class)
*/

        /*...*/

        public class EmployeeTest {

            /*...*/

            public void givenNone_returnBooleanAdminStatusFalse() {
                boolean actualResult = employee.isAdmin();
                /*...*/
            }

            @Test
            public void givenLessThanZero_thenReturnIllegalArgumentException() {
                assertThrows(IllegalArgumentException.class, () -> {
                    employee.getSalaryGrade(-1);
                });
            }

        }

/* 
12. Add more unit tests for the "getSalaryGrade" method to capture all possible outcomes.
    Application > src > test > java > EmployeeTest.java (Class)
*/

        /*...*/

        public class EmployeeTest {
            
            /*...*/

            @Test
            public void givenLessThanZero_thenReturnIllegalArgumentException() {
                /*...*/
            }

            @Test
            public void givenTenThousandAsSalary_thenReturnSalaryGradeAsOne() {
                int actualResult = employee.getSalaryGrade(10000);
                int expectedResult = 1;
                assertEquals(expectedResult, actualResult);
            }

            @Test
            public void givenTwentyTwoThousandAsSalary_thenReturnSalaryGradeAsTwo() {
                int actualResult = employee.getSalaryGrade(22000);
                int expectedResult = 2;
                assertEquals(expectedResult, actualResult);
            }

            @Test
            public void givenFiftyThousandAsSalary_thenReturnSalaryGradeAsThree() {
                int actualResult = employee.getSalaryGrade(50000);
                int expectedResult = 3;
                assertEquals(expectedResult, actualResult);
            }

            @Test
            public void givenThirtyThousandAsSalary_thenReturnNotEqualToOne() {
                int actualResult = employee.getSalaryGrade(30000);
                int expectedResult = 1;
                assertNotEquals(expectedResult, actualResult);
            }

        }

        /*
        Important Note:
            - For the "assertThrows" and "assertNotEquals" methods hover over the methods and select "more actions" > "import static method" > "org.junit.jupiter..."
        */

/*
========
Activity
========
*/

/* 
1. Create a GitLab project in the trainees "resources" sub folder providing the contents of the "template" Project provided in the materials repository and give it the name "activity".
    GitLab Repository > resources > VRT001-S09 > activity
*/

        /*
        Important Note:
            - Refer to the "template" Project folder for the codebase to be provided to the students/trainees.
        */

/*
2. Have the students clone the repository.
    GitLab Repository > resources > VRT001-S09 > activity
*/

/*
3. Check the solution using the "activity" code base provided for the session.
    VRT001-S09 > activity
*/