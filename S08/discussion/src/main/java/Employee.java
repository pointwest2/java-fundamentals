public class Employee {

    private String firstName;
    private String lastName;
    private String department;
    private String hireDate;
    private boolean isAdmin;

    public Employee() {
    }

    public Employee(String firstName, String lastName, String department, String hireDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.department = department;
        this.hireDate = hireDate;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public String printFullName() {
        return firstName + " " + lastName;
    }

    // Method to store data in a database
    public String create(String firstName, String lastName, String department, String hireDate) {

        String result;

        if (firstName.trim().length() == 0 || lastName.trim().length() == 0 || department.trim().length() == 0 || hireDate.trim().length() == 0) {
            result = "The first name, last name, department and hireDate should not be empty.";
        } else {
            result = "Employee successfully created.";
        }

        return result;
    }

    public int getSalaryGrade(int salary) {
        // Test salary range and return salary grade
        if (salary < 10000) {
            throw new IllegalArgumentException("Salary must be greater or equal to 10000.");
        } else {
            if (salary >= 10000 && salary <= 20000) {
                return 1;
            }
            else if (salary > 20000 && salary <= 30000) {
                return 2;
            }
            else {
                return 3;
            }
        }
    }

}
