import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

/*
* The "@DisplayNameGeneration" annotation allows custom display names for the
* whole class removing repetitive "@DisplayName" annotations.
* This is best used with certain naming conventions to make the display names be more readable.
*/
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
/*
* Naming convention for Test classes is the name of the class and the suffix of
* "Test" for code readability
*/
public class EmployeeTest {

    // Property to be used for all test cases
    Employee employee;

    /*
    * The "@BeforeEach" annotation allows execution of code before running a test
    * This is best used for common code execution.
    * Runs before each test to instantiate an employee object.
    * The "@Before" annotation was used for JUnit 4.
    */
    @BeforeEach
    void instantiateEmployee() {
        employee = new Employee("John", "Doe", "Engineering", "2021-10-24");
    }

    /*
    * The "@BeforeAll" functions similarly as the "@BeforeEach" annotation but
    * is preferably used for common expensive operations to be executed once
    * before running all tests such as connecting to a database.
    * "setUp" was a required naming convention for methods annotated with
    * "@Before" using JUnit 3. As of JUnit 4 and onwards, using meaningful names
    * or names based on the annotation are acceptable.
    * The "@BeforeClass" annotation was used for JUnit 4.
    */
    @BeforeAll
    public static void setUp() {
        System.out.println("Connected to database.");
    }

    /*
    * The "@AfterAll" annotation executes code after each test and was a
    * required naming convention as of JUnit 3.
    */
    @AfterAll
    public static void tearDown() {
        System.out.println("Closing database connection.");
    }

    // A method annotated with @Test defines a test method
    @Test
    /*
    * The "@DisplayName" annotation can be used to declare a custom display name
    * for code readability.
    */
    @DisplayName("Print the employee's first and last name as a single string.")
    /*
    * One naming convention for test methods can be in the pattern of:
    * [UnitOfWork_StateUnderTest_ExpectedBehavior]
    */
    public void printFullName_employeeProperties_returnsStringFullName() {
        /*
        * Expected vs. Actual
        * A test case should have an assertion between the expected and actual values.
        * It's suggested to prefix the variable names with the words "actual"
        * and "expected" keywords to improve code readability
        */
        String actualResult = employee.printFullName();
        String expectedResult = "John Doe";
        assertEquals(expectedResult, actualResult);
    }

    @Test
    @DisplayName("Ensure all properties are not empty.")
    public void givenEmployeeDetails_returnsCreateErrorMessage() {
        String actualResult = employee.create("Jane", "Doe", "HR", " ");
        String expectedResult = "The first name, last name, department and hireDate should not be empty.";
        assertEquals(expectedResult, actualResult);
    }

    /*
    * It is best practice to create unit tests for all possible outcomes to
    * capture any errors that may be encountered.
    * Alternating between code modification and unit testing is a good way to
    * ensure that all results are accounted for.
    */
    @Test
    // @DisplayName("Successful employee creation.")
    public void givenEmployeeDetails_returnsCreateSuccessMessage() {
        String actualResult = employee.create("Jane", "Doe", "HR", "2021-05-01");
        String expectedResult = "Employee successfully created.";
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void givenNone_returnBooleanAdminStatusFalse() {
        boolean actualResult = employee.isAdmin();
        assertFalse(actualResult);
    }

    @Test
    public void givenLessThanZero_thenReturnIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> {
            employee.getSalaryGrade(-1);
        });
    }

    @Test
    public void givenTenThousandAsSalary_thenReturnSalaryGradeAsOne() {
        int actualResult = employee.getSalaryGrade(10000);
        int expectedResult = 1;
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void givenTwentyTwoThousandAsSalary_thenReturnSalaryGradeAsTwo() {
        int actualResult = employee.getSalaryGrade(22000);
        int expectedResult = 2;
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void givenFiftyThousandAsSalary_thenReturnSalaryGradeAsThree() {
        int actualResult = employee.getSalaryGrade(50000);
        int expectedResult = 3;
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void givenThirtyThousandAsSalary_thenReturnNotEqualToOne() {
        int actualResult = employee.getSalaryGrade(30000);
        int expectedResult = 1;
        assertNotEquals(expectedResult, actualResult);
    }

}
